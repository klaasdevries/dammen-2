use ai::{MinimaxAI, AI};
use criterion::{criterion_group, criterion_main, Criterion};
use dammen_core::game::GameNode;

fn haarlemmer(c: &mut Criterion) {
    let string = "Black 2
        | |b| |b| |b| |b| |b|
        |b| |b| |b| |b| |b| |
        | |b| |b| |b| |b| |b|
        |b| |b| |.| |b| |b| |
        | |.| |.| |b| |.| |.|
        |.| |.| |w| |.| |.| |
        | |w| |w| |w| |w| |w|
        |w| |.| |w| |w| |w| |
        | |w| |w| |w| |w| |w|
        |w| |w| |w| |w| |w| |
        ";

    let game = GameNode::from_string(string).unwrap();
    c.bench_function("haarlemmer", move |b| {
        let mut ai = MinimaxAI::new(3);
        b.iter(|| {
            let mv = ai.best_move(&game);
            assert_eq!(vec![23, 29], mv.unwrap().to_vec());
        })
    });
}

criterion_group!(benches, haarlemmer,);
criterion_main!(benches);
