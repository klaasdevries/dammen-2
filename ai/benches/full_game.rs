use ai::{MTMinimaxAI, MinimaxAI, RandomAI, AI};
use criterion::{criterion_group, criterion_main, Criterion, SamplingMode};
use dammen_core::game::GameNode;

fn full_game_random(c: &mut Criterion) {
    c.bench_function("full game random", move |b| {
        b.iter(|| {
            let mut game = GameNode::initial();
            let mut ai = RandomAI::from_seed(42);
            while let Some(mv) = ai.best_move(&game) {
                game = game.apply(mv);
            }
        })
    });
}

fn full_game_minimax(c: &mut Criterion) {
    const DEPTH: u32 = 7;
    let mut group = c.benchmark_group("minimax");
    group.sample_size(10);
    group.sampling_mode(SamplingMode::Flat);
    group.warm_up_time(std::time::Duration::from_secs(10));
    group.measurement_time(std::time::Duration::from_secs(30));

    group.bench_function("full game minimax", move |b| {
        b.iter(|| {
            let mut game = GameNode::initial();
            let mut ai = MinimaxAI::new(DEPTH);
            for _ in 0..100 {
                if let Some(mv) = ai.best_move(&game) {
                    game = game.apply(mv);
                } else {
                    break;
                }
            }
        })
    });
    group.finish();
}

fn full_game_mt_minimax(c: &mut Criterion) {
    const DEPTH: u32 = 7;
    let mut group = c.benchmark_group("minimax");
    group.sample_size(10);
    group.sampling_mode(SamplingMode::Flat);
    group.warm_up_time(std::time::Duration::from_secs(10));
    group.measurement_time(std::time::Duration::from_secs(30));

    group.bench_function("full game minimax - mt", move |b| {
        b.iter(|| {
            let mut game = GameNode::initial();
            let mut ai = MTMinimaxAI::new(DEPTH);
            for _ in 0..100 {
                if let Some(mv) = ai.best_move(&game) {
                    game = game.apply(mv);
                } else {
                    break;
                }
            }
        })
    });
    group.finish();
}

criterion_group!(
    benches,
    full_game_random,
    full_game_minimax,
    full_game_mt_minimax
);
criterion_main!(benches);
