use crate::{evaluator, memo};
use evaluator::win_condition;

use dammen_core::game::{GameNode, Move, Side};
use evaluator::Evaluator;
use rayon::prelude::*;

pub fn minimax<E: Evaluator>(
    memo: &memo::AsyncMemo,
    eval: &E,
    game: GameNode,
    depth: u32,
    alpha: i32,
    beta: i32,
) -> (Option<Move>, i32) {
    match memo.get(game, depth) {
        None => {
            let (mv, val) = minimax_impl(memo, eval, game, depth, alpha, beta);
            memo.insert(game, mv, val, depth);
            (mv, val)
        }
        Some((mv, val)) => (mv, val),
    }
}

pub fn minimax_mt<E: Evaluator + Sync>(
    memo: &memo::AsyncMemo,
    eval: &E,
    game: GameNode,
    depth: u32,
    alpha: i32,
    beta: i32,
) -> (Option<Move>, i32) {
    use atomic::Ordering::Relaxed;
    use std::sync::atomic;

    const OBVIOUS_CONTINUATION: usize = 2;
    const MT_CUTOFF_DEPTH: u32 = 4; // For shallow (sub)trees single threaded minimax can actually be faster; better alpha-beta pruning and less synchronisation overhead. The cutoff is determined experimentally.

    if depth <= MT_CUTOFF_DEPTH {
        minimax(memo, eval, game, depth, alpha, beta)
    } else if let Some(item) = memo.get(game, depth) {
        item
    } else {
        let cmp = match game.movenumber().1 {
            Side::White => |a: i32, b: i32| a.cmp(&b),
            Side::Black => |a: i32, b: i32| b.cmp(&a),
        };

        let next = game.next();
        let new_depth = if next.len() <= OBVIOUS_CONTINUATION {
            depth
        } else {
            depth - 1
        }; // obvious continuations should not tax the depth too much

        let cancel = atomic::AtomicBool::new(false);
        let alpha = atomic::AtomicI32::new(alpha);
        let beta = atomic::AtomicI32::new(beta);

        let best = next
            .as_slice()
            .par_iter()
            .map(|mv| {
                if cancel.load(Relaxed) {
                    return (None, 0);
                }

                let new_game = game.apply(*mv);
                let (_, value) = minimax_mt(
                    memo,
                    eval,
                    new_game,
                    new_depth,
                    alpha.load(Relaxed),
                    beta.load(Relaxed),
                );
                (Some(*mv), value)
            })
            .filter(|(mv, _)| mv.is_some())
            .map(|(mv, value)| {
                if game.movenumber().1 == Side::White {
                    if value >= beta.load(Relaxed) {
                        cancel.store(true, Relaxed);
                    }
                    alpha.fetch_max(value, Relaxed);
                } else {
                    if value <= alpha.load(Relaxed) {
                        cancel.store(true, Relaxed);
                    }
                    beta.fetch_min(value, Relaxed);
                }
                (mv, value)
            })
            .max_by(|a, b| cmp(a.1, b.1))
            .or_else(|| Some((None, win_condition(&game))))
            .unwrap();

        memo.insert(game, best.0, best.1, depth);
        best
    }
}

fn minimax_impl<E: Evaluator>(
    memo: &memo::AsyncMemo,
    eval: &E,
    game: GameNode,
    depth: u32,
    mut alpha: i32,
    mut beta: i32,
) -> (Option<Move>, i32) {
    const OBVIOUS_CONTINUATION: usize = 2;

    if depth == 0 {
        (None, eval.evaluate(&game))
    } else {
        let cmp = match game.movenumber().1 {
            Side::White => |a: i32, b: i32| a.cmp(&b),
            Side::Black => |a: i32, b: i32| b.cmp(&a),
        };

        let next = game.next();
        let new_depth = if next.len() <= OBVIOUS_CONTINUATION {
            depth
        } else {
            depth - 1
        }; // obvious continuations should not tax the depth too much

        let mut best = (None, win_condition(&game));
        for mv in next.iter() {
            let next_game = game.apply(*mv);

            let value = minimax(memo, eval, next_game, new_depth, alpha, beta).1;
            if cmp(value, best.1) == std::cmp::Ordering::Greater {
                best = (Some(*mv), value);
            }

            if game.movenumber().1 == Side::White {
                if value >= beta {
                    break;
                }
                alpha = std::cmp::max(alpha, value);
            } else {
                if value <= alpha {
                    break;
                }
                beta = std::cmp::min(beta, value);
            }
        }
        best
    }
}
