pub mod evaluator;
mod memo;
mod minimax;

use dammen_core::{
    game::{GameNode, Move},
    manager::{Context, Player},
};
pub use evaluator::{DefaultEvaluator, Evaluator};

pub trait AI {
    fn best_move(&mut self, game: &GameNode) -> Option<Move>;
}

pub struct MinimaxAI {
    eval: DefaultEvaluator,
    depth: u32,
    memo: memo::AsyncMemo,
}

impl MinimaxAI {
    pub fn new(depth: u32) -> MinimaxAI {
        MinimaxAI::with_evaluator(depth, DefaultEvaluator::new())
    }

    pub fn with_evaluator(depth: u32, eval: DefaultEvaluator) -> MinimaxAI {
        MinimaxAI {
            eval,
            depth,
            memo: memo::AsyncMemo::new(),
        }
    }

    fn minimax(
        &mut self,
        game: GameNode,
        depth: u32,
        alpha: i32,
        beta: i32,
    ) -> (Option<Move>, i32) {
        minimax::minimax(&self.memo, &self.eval, game, depth, alpha, beta)
    }
}

impl AI for MinimaxAI {
    fn best_move(&mut self, game: &GameNode) -> Option<Move> {
        let nxt = game.next();
        if nxt.len() == 1 {
            return Some(nxt[0]);
        }

        self.memo.purge(*game);

        self.minimax(*game, self.depth, i32::MIN, i32::MAX).0
    }
}

impl Player for MinimaxAI {
    fn choose(&mut self, game: &GameNode, _: &Context) -> Option<Move> {
        self.best_move(game)
    }
}

pub struct MTMinimaxAI {
    eval: DefaultEvaluator,
    depth: u32,
    memo: memo::AsyncMemo,
}

impl MTMinimaxAI {
    pub fn new(depth: u32) -> MTMinimaxAI {
        MTMinimaxAI::with_evaluator(depth, DefaultEvaluator::new())
    }

    pub fn with_evaluator(depth: u32, eval: DefaultEvaluator) -> MTMinimaxAI {
        MTMinimaxAI {
            eval,
            depth,
            memo: memo::AsyncMemo::new(),
        }
    }
}

impl AI for MTMinimaxAI {
    fn best_move(&mut self, game: &GameNode) -> Option<Move> {
        let nxt = game.next();
        if nxt.len() == 1 {
            return Some(nxt[0]);
        }
        self.memo.purge(*game);

        minimax::minimax_mt(
            &self.memo,
            &self.eval,
            *game,
            self.depth,
            i32::MIN,
            i32::MAX,
        )
        .0
    }
}

impl Player for MTMinimaxAI {
    fn choose(&mut self, game: &GameNode, _: &Context) -> Option<Move> {
        self.best_move(game)
    }
}

pub struct RandomAI {}

impl RandomAI {
    pub fn new() -> RandomAI {
        RandomAI {}
    }

    pub fn from_seed(s: u64) -> RandomAI {
        fastrand::seed(s);
        RandomAI::new()
    }
}

impl Default for RandomAI {
    fn default() -> Self {
        RandomAI::new()
    }
}

impl AI for RandomAI {
    fn best_move(&mut self, game: &GameNode) -> Option<Move> {
        let next = game.next();
        if next.is_empty() {
            return None;
        }

        let choice = fastrand::usize(0..next.len());
        Some(next[choice])
    }
}

impl Player for RandomAI {
    fn choose(&mut self, game: &GameNode, _: &Context) -> Option<Move> {
        self.best_move(game)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn random_game() {
        let mut game = GameNode::initial();
        let mut ai = RandomAI::from_seed(42);

        let mut count = 0;
        while let Some(mv) = ai.best_move(&game) {
            game = game.apply(mv);
            count += 1;
        }

        assert!(count >= 77);
    }

    #[test]
    fn picks_obvious_best_move() {
        let string = "White 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |b| |.| |.| |.|
        |.| |.| |.| |b| |b| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |w| |w| |
        | |.| |.| |.| |.| |w|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();
        let mut ai = MinimaxAI::new(3);

        assert_eq!(vec![24, 30], ai.best_move(&game).unwrap().to_vec());
    }

    #[test]
    fn picks_winning_move() {
        let string = "White 1
        | |.| |.| |.| |.| |B|
        |w| |.| |.| |.| |.| |
        | |.| |.| |.| |w| |w|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();
        let mut ai = MinimaxAI::new(3);

        assert_eq!(vec![10, 15], ai.best_move(&game).unwrap().to_vec());
    }

    #[test]
    fn picks_haarlemmer() {
        let string = "Black 2
        | |b| |b| |b| |b| |b|
        |b| |b| |b| |b| |b| |
        | |b| |b| |b| |b| |b|
        |b| |b| |.| |b| |b| |
        | |.| |.| |b| |.| |.|
        |.| |.| |w| |.| |.| |
        | |w| |w| |w| |w| |w|
        |w| |.| |w| |w| |w| |
        | |w| |w| |w| |w| |w|
        |w| |w| |w| |w| |w| |
        ";

        let game = GameNode::from_string(string).unwrap();
        let mut ai = MinimaxAI::new(3);

        let mv = ai.best_move(&game).unwrap();
        assert_eq!(vec![23, 29], mv.to_vec());
        let game = game.apply(mv);

        let game = game.apply(game.next()[1]);
        println!("{}", game);

        let mv = ai.best_move(&game).unwrap();
        assert_eq!(vec![17, 22], mv.to_vec());
    }

    #[test]
    fn mt_picks_haarlemmer() {
        let string = "Black 2
        | |b| |b| |b| |b| |b|
        |b| |b| |b| |b| |b| |
        | |b| |b| |b| |b| |b|
        |b| |b| |.| |b| |b| |
        | |.| |.| |b| |.| |.|
        |.| |.| |w| |.| |.| |
        | |w| |w| |w| |w| |w|
        |w| |.| |w| |w| |w| |
        | |w| |w| |w| |w| |w|
        |w| |w| |w| |w| |w| |
        ";

        let game = GameNode::from_string(string).unwrap();
        let mut ai = MTMinimaxAI::new(3);

        let mv = ai.best_move(&game).unwrap();
        assert_eq!(vec![23, 29], mv.to_vec());
        let game = game.apply(mv);

        let game = game.apply(game.next()[1]);
        println!("{}", game);

        let mv = ai.best_move(&game).unwrap();
        assert_eq!(vec![17, 22], mv.to_vec());
    }
}
