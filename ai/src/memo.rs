use dammen_core::game::{GameNode, GameNodeHashVal, Move};

pub struct AsyncMemo {
    store: dashmap::DashMap<GameNodeHashVal, (GameNode, Option<Move>, i32, u32)>,
}

impl AsyncMemo {
    pub fn new() -> AsyncMemo {
        AsyncMemo {
            store: dashmap::DashMap::new(),
        }
    }

    pub fn get(&self, key: GameNode, depth: u32) -> Option<(Option<Move>, i32)> {
        let gh = key.hash_val();

        if let Some(item) = self.store.get(&gh) {
            let value = item.value();
            if value.3 >= depth {
                return Some((value.1, value.2));
            }
        }
        None
    }

    pub fn insert(&self, game: GameNode, mv: Option<Move>, value: i32, depth: u32) {
        let gh = game.hash_val();
        self.store.insert(gh, (game, mv, value, depth));
    }

    pub fn purge(&self, game: GameNode) {
        // clear cache: remove items we obviously don't need (have more pieces on the board)
        let (gw, gb, _) = game.counts();
        let gw = gw.count_ones();
        let gb = gb.count_ones();

        self.store.retain(|_, (g, _, _, _)| {
            let (white, black, _) = g.counts();
            white.count_ones() <= gw && black.count_ones() <= gb
        });
    }
}

impl Default for AsyncMemo {
    fn default() -> AsyncMemo {
        Self::new()
    }
}
