use dammen_core::{
    bittwiddling,
    game::{GameNode, Side},
};

macro_rules! field_mask {
    ($n:expr) => {
        1u64 << $n
    };
    ($head:expr, $($tail:expr),+) => {
        field_mask!($head) | field_mask!($($tail),+)
    }
}

pub trait Evaluator {
    fn evaluate(&self, game: &GameNode) -> i32;
}

pub const NUM_WEIGHTS: usize = 5;

#[derive(Clone)]
pub struct DefaultEvaluator {
    weights: [u32; NUM_WEIGHTS],
}

impl DefaultEvaluator {
    pub fn new() -> DefaultEvaluator {
        DefaultEvaluator::with_weights([18, 16, 12, 6, 0])
    }

    pub fn with_weights(weights: [u32; NUM_WEIGHTS]) -> DefaultEvaluator {
        DefaultEvaluator { weights }
    }
}
impl Default for DefaultEvaluator {
    fn default() -> Self {
        Self::new()
    }
}

impl Evaluator for DefaultEvaluator {
    fn evaluate(&self, game: &GameNode) -> i32 {
        let (white, black, _) = game.counts();
        if white.count_ones() == 0 {
            return i32::MIN;
        }
        if black.count_ones() == 0 {
            return i32::MAX;
        }

        const VALUATORS: [fn(&GameNode) -> i32; NUM_WEIGHTS] =
            [count_queens, count_pieces, central, compact, variation];

        VALUATORS
            .iter()
            .map(|valuator| valuator(game))
            .zip(self.weights.iter())
            .map(|(score, weight)| score << weight)
            .sum()
    }
}

pub fn win_condition(game: &GameNode) -> i32 {
    match (game.is_regulatory_draw(), game.movenumber().1) {
        (false, Side::White) => i32::MIN,
        (false, Side::Black) => i32::MAX,
        (true, _) => 0,
    }
}

fn count_pieces(game: &GameNode) -> i32 {
    let (white, black, _) = game.counts();
    let white_count = white.count_ones();
    let black_count = black.count_ones();

    (white_count as i32) - (black_count as i32)
}

fn count_queens(game: &GameNode) -> i32 {
    let (white, black, queens) = game.counts();
    let white_count = (white & queens).count_ones();
    let black_count = (black & queens).count_ones();

    (white_count as i32) - (black_count as i32)
}

fn central(game: &GameNode) -> i32 {
    const MASK: u64 =
        field_mask!(11, 12, 13, 16, 17, 18, 21, 22, 23, 26, 27, 28, 31, 32, 33, 36, 37, 38);

    let (white, black, _) = game.counts();
    let white = (white & MASK).count_ones();
    let black = (black & MASK).count_ones();

    (white as i32) - (black as i32)
}

fn compact(game: &GameNode) -> i32 {
    use bittwiddling::*;

    let (white, black, _) = game.counts();

    (sw_off(white) & white).count_ones() as i32 + (se_off(white) & white).count_ones() as i32
        - (ne_off(black) & black).count_ones() as i32
        - (nw_off(black) & black).count_ones() as i32
}

fn variation(_: &GameNode) -> i32 {
    fastrand::i8(..) as i32
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn white_wins_maximizes() {
        let string = "Black 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();
        let eval = DefaultEvaluator::new();

        assert_eq!(i32::MAX, eval.evaluate(&game));
    }

    #[test]
    fn black_wins_minizes() {
        let string = "White 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |b| |.| |.| |.| |
        | |b| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();
        let eval = DefaultEvaluator::new();

        assert_eq!(i32::MIN, eval.evaluate(&game));
    }

    #[test]
    fn pieces_are_counted() {
        let string = "White 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |b| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |w| |.| |.| |
        | |.| |w| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();
        let eval = DefaultEvaluator::new();

        assert!(eval.evaluate(&game) > 0);
    }

    #[test]
    fn pieces_are_counted_queens_are_worth_more() {
        let string = "White 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |B| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |w| |.| |.| |
        | |.| |w| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();
        let eval = DefaultEvaluator::new();

        assert!(eval.evaluate(&game) < 0);
    }

    #[test]
    fn variation_is_somewhat_random() {
        use std::collections::HashSet;
        fastrand::seed(13);

        let game = GameNode::initial();
        let seen: HashSet<i32> = (0..20).map(|_| variation(&game)).collect();

        assert!(seen.len() >= 18);
    }

    #[test]
    fn central_positions_are_promoted() {
        fastrand::seed(13);

        let one = "White 1
        | |b| |.| |.| |b| |.|
        |b| |.| |b| |b| |.| |
        | |.| |b| |b| |.| |b|
        |.| |.| |.| |.| |.| |
        | |.| |w| |.| |.| |.|
        |.| |w| |w| |.| |.| |
        | |.| |w| |w| |.| |.|
        |.| |w| |w| |w| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let two = "White 1
        | |b| |.| |.| |b| |.|
        |b| |.| |b| |b| |.| |
        | |.| |b| |b| |.| |b|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |w|
        |w| |.| |.| |.| |.| |
        | |w| |.| |w| |.| |.|
        |w| |.| |.| |w| |.| |
        | |.| |.| |.| |.| |.|
        |.| |w| |w| |.| |.| |
        ";

        let one = GameNode::from_string(one).unwrap();
        let two = GameNode::from_string(two).unwrap();

        let eval = DefaultEvaluator::new();

        assert!(eval.evaluate(&one) > 0);
        assert!(eval.evaluate(&two) < 0);
    }

    #[test]
    fn compact_positions_are_promoted() {
        fastrand::seed(13);

        let one = "White 1
        | |b| |.| |.| |b| |.|
        |b| |.| |b| |b| |.| |
        | |.| |b| |b| |.| |b|
        |.| |.| |.| |.| |.| |
        | |.| |w| |.| |.| |.|
        |.| |w| |w| |.| |.| |
        | |.| |w| |w| |.| |.|
        |.| |w| |w| |w| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let two = "White 1
        | |b| |.| |.| |b| |.|
        |b| |.| |b| |b| |.| |
        | |.| |b| |b| |w| |b|
        |.| |w| |.| |.| |.| |
        | |.| |.| |w| |w| |.|
        |.| |w| |.| |.| |.| |
        | |.| |.| |w| |.| |.|
        |.| |w| |.| |w| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let one = GameNode::from_string(one).unwrap();
        let two = GameNode::from_string(two).unwrap();

        let eval = DefaultEvaluator::new();

        assert!(eval.evaluate(&one) > eval.evaluate(&two));
    }
}
