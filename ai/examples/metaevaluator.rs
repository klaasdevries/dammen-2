use ai::{evaluator::NUM_WEIGHTS, DefaultEvaluator, MinimaxAI};
use dammen_core::manager::{GameManager, Winner};
use std::sync::mpsc;
type Result<T> = anyhow::Result<T>;
use anyhow::Error;
use clap::Parser;

macro_rules! assert_gt {
    ($left:expr, $right:expr) => {
        let l = $left;
        let r = $right;
        if !(l > r) {
            panic!("assertion failed: ! {:?} > {:?}", l, r);
        }
    };
}
/// metaevalator
#[derive(Parser, Debug)]
struct Args {
    /// number of games to play
    #[arg(short, long, default_value = "100")]
    num_games: u32,

    /// depth of the ai
    #[arg(short, long, default_value = "5")]
    depth: u32,

    /// weights to use
    #[arg(short, long, default_value = "18,16,12,6,0")]
    weights: String,
}

fn main() {
    let args = Args::parse();

    let depth = args.depth;
    let num_games = args.num_games;
    let weights = extract_array(&args.weights)
        .unwrap_or_else(|_| panic!("{} comma-separated numbers", NUM_WEIGHTS));

    let baseline = DefaultEvaluator::new();
    let candidate = DefaultEvaluator::with_weights(weights);

    let (tx, rx) = mpsc::channel();

    (0..num_games).for_each(|i| {
        let b = Box::new(MinimaxAI::with_evaluator(depth, baseline.clone()));
        let c = Box::new(MinimaxAI::with_evaluator(depth, candidate.clone()));

        let (white, black, m) = if i & 1 == 0 { (b, c, -1) } else { (c, b, 1) };

        let tx = tx.clone();
        rayon::spawn(move || {
            let s = match GameManager::new(white, black).play() {
                Winner::White => 1,
                Winner::Black => -1,
                Winner::Draw(_) => 0,
            };
            let s = s * m;
            println!("{:2}", s);
            tx.send(s).unwrap();
        })
    });

    drop(tx);

    let score: i32 = rx.iter().sum();

    println!("score: {}", score);
    assert_gt!(score, 0);
}

fn extract_uint(v: &str) -> Result<u32> {
    let num = v.parse::<u32>()?;
    Ok(num)
}

fn extract_array(v: &str) -> Result<[u32; NUM_WEIGHTS]> {
    let as_vec: Result<Vec<_>> = v.split(',').map(extract_uint).collect();
    match as_vec {
        Ok(av) => match av.as_slice().try_into() {
            Ok(a) => Ok(a),
            Err(e) => Err(Error::msg(e)),
        },
        Err(e) => Err(e),
    }
}
