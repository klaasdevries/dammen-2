use dammen_core::{
    game::{GameNode, Move, Side, State, Winner},
    manager::GameManager,
};
use egui::*;

mod file_dialog;
mod player;

use file_dialog::{LoadFileTrait, SaveFileTrait};
use player::*;

pub struct DammenApp {
    game: GameState,

    // players
    executor: player::AIExecutor,
    players: PlayerState,

    // ui state
    is_reversed: bool,
    input_state: InputState,

    // dialogs
    save_file_dialog: Option<file_dialog::SaveFileDialog>,
    open_file_dialog: Option<file_dialog::LoadFileDialog>,
    new_game_input: (NewPlayerInput, GameState),
    show_new_game_dialog: bool,
}

impl Default for DammenApp {
    fn default() -> Self {
        Self::with(GameNode::initial(), NewPlayerInput::default())
    }
}

impl DammenApp {
    pub fn new(_cc: &eframe::CreationContext<'_>) -> Self {
        // This is also where you can customize the look and feel of egui using
        // `cc.egui_ctx.set_visuals` and `cc.egui_ctx.set_fonts`.
        Default::default()
    }

    pub fn with(start: GameNode, players: NewPlayerInput) -> Self {
        Self {
            game: GameState::with_initial(start),
            executor: player::AIExecutor::new(),
            players: PlayerState::from(&players),

            is_reversed: false,
            input_state: InputState::new(start),

            save_file_dialog: None,
            open_file_dialog: None,
            new_game_input: (NewPlayerInput::default(), GameState::new()),
            show_new_game_dialog: false,
        }
    }

    fn set_from_input(&mut self) {
        self.game = GameState::with_initial(*self.new_game_input.1.manager.get_game());

        self.players = PlayerState::from(&self.new_game_input.0);
        self.input_state.clear(*self.game.manager.get_game());

        self.executor = AIExecutor::new();
    }

    fn game_state(&self) -> &GameNode {
        self.game.manager.get_game()
    }

    fn side_to_move(&self) -> Side {
        self.game.side_to_move()
    }

    fn advance(&mut self, mv: Option<Move>) {
        assert!(self.game.outcome.is_none());

        match mv {
            Some(m) => {
                log::info!("move {}", self.game_state().find_matching_rich(m));
            }
            None => {
                log::debug!("{:?} resigning", self.side_to_move());
            }
        }

        if let Some(o) = self.game.advance(mv) {
            log::info!("outcome: {:?}", o);
        }

        self.input_state.do_move(mv, *self.game.manager.get_game());
    }

    fn new_game_input_dialog(&mut self, ctx: &Context, max_size: Vec2) {
        egui::Window::new(NEW_GAME)
            .id("new-game-dialog".into())
            .title_bar(true)
            .collapsible(false)
            .resizable(false)
            .max_size(max_size)
            .show(ctx, |ui| {
                ui.vertical_centered_justified(|ui| {
                    ui.horizontal(|ui| {
                        ui.with_layout(Layout::left_to_right(Align::RIGHT), |ui| {
                            if self.is_reversed {
                                ui.add(piece_widget(State::Black));

                                ui.add(SetPlayer::new(
                                    &mut self.new_game_input.0.black,
                                    &mut self.new_game_input.0.black_depth,
                                ));
                            } else {
                                ui.add(piece_widget(State::White));

                                ui.add(SetPlayer::new(
                                    &mut self.new_game_input.0.white,
                                    &mut self.new_game_input.0.white_depth,
                                ));
                            }
                        });
                    });

                    ui.horizontal(|ui| {
                        let board_size = ui.available_width();
                        let node = self.new_game_input.1.manager.get_game();
                        draw_board(ui, node, board_size, self.is_reversed);
                    });

                    ui.horizontal(|ui| {
                        if !self.is_reversed {
                            ui.add(piece_widget(State::Black));

                            ui.add(SetPlayer::new(
                                &mut self.new_game_input.0.black,
                                &mut self.new_game_input.0.black_depth,
                            ));
                        } else {
                            ui.add(piece_widget(State::White));

                            ui.add(SetPlayer::new(
                                &mut self.new_game_input.0.white,
                                &mut self.new_game_input.0.white_depth,
                            ));
                        }
                    });

                    ui.horizontal(|ui| {
                        let min_size = Vec2::splat(ui.spacing().interact_size.y * 1.5);

                        let cancel = Button::new(CANCEL).min_size(min_size);
                        let engage = Button::new(NEW_GAME).min_size(min_size);

                        if ui
                            .add_sized(min_size, cancel)
                            .on_hover_text("Cancel")
                            .clicked()
                        {
                            log::info!("cancel new game");
                            self.show_new_game_dialog = false;
                        }

                        ui.with_layout(Layout::right_to_left(Align::RIGHT), |ui| {
                            if ui
                                .add_sized(min_size, engage)
                                .on_hover_text("Engage!")
                                .clicked()
                            {
                                log::info!("start new game");
                                self.show_new_game_dialog = false;

                                self.set_from_input();
                            }
                        });
                    });
                })
            });
    }

    fn draw_board(&mut self, ui: &mut Ui) {
        let node = *self.game_state();

        let player = match self.side_to_move() {
            Side::Black => &mut self.players.black,
            Side::White => &mut self.players.white,
        };

        let clickables = match player {
            Player::Robot(r) => {
                if self.game.outcome.is_none() {
                    if !self.executor.is_working() {
                        self.executor.dispatch(r.depth(), node);
                        if self.executor.is_blocking() {
                            ui.ctx()
                                .output_mut(|o| o.cursor_icon = egui::CursorIcon::Wait);
                        }
                    } else {
                        if let Some(mv) = self.executor.poll() {
                            self.advance(mv)
                        }
                        ui.ctx()
                            .request_repaint_after(std::time::Duration::from_millis(50));
                    }
                }
                std::collections::HashSet::new()
            }
            Player::Human => {
                let candidates = self.input_state.match_candidates(self.game_state());
                if self.input_state.try_select() && candidates.len() == 1 {
                    self.advance(Some(candidates[0]));
                    ui.ctx().request_repaint();
                }

                candidates.iter().flat_map(|m| m.to_vec()).collect()
            }
            Player::Random(r) => {
                use ai::AI;
                if self.game.outcome.is_none() {
                    self.game.advance(r.best_move(&node));
                }
                std::collections::HashSet::new()
            }
        };

        let style = Style::new(ui.style());
        let board_size = ui.available_width().min(ui.available_height());
        let square_size = board_size / 10.0;

        let (response, painter) =
            ui.allocate_painter(Vec2::new(board_size, board_size), Sense::click_and_drag());

        let to_screen = emath::RectTransform::from_to(
            Rect::from_min_size(Pos2::ZERO, response.rect.size()),
            response.rect,
        );

        let board_rect = Rect::from_min_max(Pos2::ZERO, Pos2::new(board_size, board_size));
        let board_rect = to_screen.transform_rect(board_rect);
        let board_rect = Shape::rect_filled(board_rect, Rounding::ZERO, style.white_board_color());
        painter.add(board_rect);

        for n in 1..=50 {
            let (x, y) = compute_xy(n - 1, self.is_reversed);

            let pos = Pos2::new(x as f32 * square_size, y as f32 * square_size);

            let field = Rect::from_min_max(pos, pos + Vec2::splat(square_size));
            let field = to_screen.transform_rect(field);
            painter.add(Shape::rect_filled(
                field,
                Rounding::ZERO,
                style.black_board_color(),
            ));

            Piece::new(self.game_state().at(n), style).draw(&field, &painter);

            if clickables.contains(&n) {
                let r = ui.interact(field, response.id.with(n), Sense::click_and_drag());
                if r.hovered() {
                    self.animate(ui, &field, &style);
                }
                if r.clicked() {
                    log::debug!("clicked field {}", n);
                    self.input_state.click(n);
                }
                if self.input_state.highlight(n) {
                    self.highlight(&field, &painter, &style);
                }
                if self.input_state.suggest(n) {
                    self.suggest(&field, &painter, &style);
                }
                if self.input_state.animate(n) {
                    self.animate(ui, &field, &style);
                }
            } else if !self.input_state.try_select() && self.input_state.last.contains(&n) {
                self.highlight(&field, &painter, &style);
            }

            painter.text(
                to_screen.transform_pos(pos + Vec2::new(square_size / 25.0, square_size)),
                Align2::LEFT_BOTTOM,
                format!("{}", n),
                FontId::proportional(square_size / 6.0),
                style.white_board_color(),
            );
        }
    }

    fn highlight(&self, field: &Rect, painter: &Painter, style: &Style) {
        let s = Shape::circle_stroke(
            field.center(),
            field.width() * 0.4,
            Stroke::new(2.0, style.highligh_color()),
        );
        painter.add(s);
    }

    fn suggest(&self, field: &Rect, painter: &Painter, style: &Style) {
        let s = Shape::circle_stroke(
            field.center(),
            field.width() * 0.4,
            Stroke::new(0.5, style.highligh_color()),
        );
        painter.add(s);
    }

    fn animate(&self, ui: &Ui, field: &Rect, style: &Style) {
        let s = Spinner::new()
            .size(field.width() * 0.1)
            .color(style.highligh_color());
        s.paint_at(ui, (*field).shrink(field.width() * 0.04));
    }

    fn is_interactive(&self) -> bool {
        match self.side_to_move() {
            Side::White => matches!(self.players.white, Player::Human),
            Side::Black => matches!(self.players.black, Player::Human),
        }
    }
}

impl eframe::App for DammenApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        ctx.input(|i| {
            if self.is_interactive() {
                if i.key_pressed(Key::Space) {
                    let reverse = i.modifiers.matches_logically(Modifiers::SHIFT);
                    self.input_state.cycle(reverse);
                    log::debug!("selected {:?}", self.input_state.selected);
                }
                if i.key_pressed(Key::Enter) {
                    log::debug!("enter pressed");
                    if let Some(mv) = self.input_state.selected {
                        self.advance(Some(mv));
                    }
                }
            }
        });

        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                if ui.button(NEW_GAME).on_hover_text("New Game").clicked() {
                    self.new_game_input.1 = GameState::new();
                    self.show_new_game_dialog = true;
                }

                if self.show_new_game_dialog {
                    let size = Vec2::splat(ui.spacing().interact_size.y * 10.0);
                    self.new_game_input_dialog(ctx, size);
                }

                if ui.button(SAVE_GAME).on_hover_text("Save game").clicked() {
                    log::info!("save game");
                    let mut content = Vec::<u8>::new();
                    match dammen_core::serialization::dump(
                        &mut content,
                        *self.game.manager.get_game(),
                        self.game.manager.get_log(),
                        &[],
                    ) {
                        Ok(_) => {
                            let dialog = file_dialog::SaveFileDialog::open(content);
                            self.save_file_dialog = Some(dialog);
                        }
                        Err(e) => {
                            log::error!("failed to serialize: {:?}", e);
                        }
                    }
                }

                if let Some(dialog) = &mut self.save_file_dialog {
                    match dialog.save(ctx) {
                        Some(Ok(_)) => {
                            log::debug!("written");
                            self.save_file_dialog = None
                        }
                        Some(Err(e)) => {
                            log::error!("write error: {:?}", e);
                            self.save_file_dialog = None
                        }
                        None => {}
                    };
                }

                if ui.button(LOAD_GAME).on_hover_text("Load game").clicked() {
                    log::info!("load game");
                    let dialog = file_dialog::LoadFileDialog::open();
                    self.open_file_dialog = Some(dialog);
                }

                if let Some(dialog) = &mut self.open_file_dialog {
                    match dialog.load(ctx) {
                        Some(Some(content)) => {
                            log::debug!("loaded content {}", content);
                            self.open_file_dialog = None;

                            match dammen_core::serialization::load_from_string(&content) {
                                Ok(node) => {
                                    self.new_game_input.1 = GameState::with_initial(node);
                                    self.show_new_game_dialog = true;
                                }
                                Err(e) => {
                                    log::error!("failed to load file: {:?}", e);
                                }
                            }
                        }
                        Some(None) => {
                            log::debug!("file loading canceled");
                            self.open_file_dialog = None;
                        }
                        None => {}
                    }
                }

                egui::widgets::global_theme_preference_switch(ui);

                if ui
                    .button(REVERSE)
                    .on_hover_text("Flip board orientation")
                    .clicked()
                {
                    self.is_reversed = !self.is_reversed;
                }
            });
        });

        egui::TopBottomPanel::bottom("bottom_panel").show(ctx, |ui| {
            let style = Style::new(&ctx.style());
            ui.horizontal(|ui| {
                let (r, p) = Piece::new(State::White, Style::new(&ctx.style())).draw_in_rect(ui);
                if self.game.outcome.is_none()
                    && self.side_to_move() == dammen_core::game::Side::White
                {
                    match self.players.white {
                        Player::Human => self.highlight(&r.rect, &p, &style),
                        _ => self.animate(ui, &r.rect, &style),
                    };
                }

                p.text(
                    r.rect.center(),
                    Align2::CENTER_CENTER,
                    format!("{}", white_count(self.game_state())),
                    FontId::proportional(r.rect.width() / 3.0),
                    style.black_piece_color(),
                );

                let (r, p) = Piece::new(State::Black, Style::new(&ctx.style())).draw_in_rect(ui);

                if self.game.outcome.is_none()
                    && self.side_to_move() == dammen_core::game::Side::Black
                {
                    match self.players.black {
                        Player::Human => self.highlight(&r.rect, &p, &style),
                        _ => self.animate(ui, &r.rect, &style),
                    };
                }

                p.text(
                    r.rect.center(),
                    Align2::CENTER_CENTER,
                    format!("{}", black_count(self.game_state())),
                    FontId::proportional(r.rect.width() / 3.0),
                    style.white_piece_color(),
                );

                let winner = match self.game.outcome {
                    Some(dammen_core::game::Winner::White) => " (2 - 0)",
                    Some(dammen_core::game::Winner::Black) => " (0 - 2)",
                    Some(dammen_core::game::Winner::Draw(_)) => " (1 - 1)",
                    None => "",
                };

                ui.label(
                    RichText::new(format!("{}{}", self.game_state().movenumber().0, winner))
                        .size(r.rect.width() / 3.0),
                );
            });
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            let style = Style::new(ui.style());

            Frame::canvas(ui.style())
                .rounding(Rounding::ZERO)
                .inner_margin(Margin::ZERO)
                .stroke(Stroke::new(1.0, style.black_piece_color()))
                .show(ui, |ui| {
                    self.draw_board(ui);
                });
        });
    }
}

struct GameState {
    manager: GameManager,
    outcome: Option<Winner>,
    white_tx: MoveTx,
    black_tx: MoveTx,
}

impl GameState {
    fn new() -> GameState {
        Self::with_initial(GameNode::initial())
    }

    fn with_initial(game: GameNode) -> GameState {
        let (white_tx, white_rx) = move_channel();
        let (black_tx, black_rx) = move_channel();

        let manager = GameManager::with_initial(Box::new(white_rx), Box::new(black_rx), game);

        GameState {
            manager,
            outcome: None,
            white_tx,
            black_tx,
        }
    }

    fn side_to_move(&self) -> Side {
        self.manager.get_game().movenumber().1
    }

    fn advance(&mut self, mv: Option<Move>) -> Option<Winner> {
        match self.side_to_move() {
            Side::White => {
                self.white_tx.set(mv);
            }
            Side::Black => {
                self.black_tx.set(mv);
            }
        };

        self.outcome = self.manager.advance();
        self.outcome
    }
}

struct PlayerState {
    white: Player,
    black: Player,
}

impl PlayerState {
    fn from(input: &NewPlayerInput) -> PlayerState {
        PlayerState {
            white: Self::from_type_and_depth(input.white, input.white_depth),
            black: Self::from_type_and_depth(input.black, input.black_depth),
        }
    }

    fn from_type_and_depth(pt: PlayerType, d: u32) -> Player {
        match pt {
            PlayerType::Human => Player::Human,
            PlayerType::Robot => Player::Robot(AIPlayer::new(d)),
            PlayerType::Random => Player::Random(ai::RandomAI::new()),
        }
    }
}

struct InputState {
    clicked: Vec<u32>,
    last: Vec<u32>,

    node: GameNode,
    selected: Option<Move>,
    select_idx: usize,
}

impl InputState {
    fn new(node: GameNode) -> Self {
        InputState {
            clicked: vec![],
            last: vec![],
            node,
            selected: None,
            select_idx: 0,
        }
    }

    fn click(&mut self, idx: u32) {
        self.clicked.push(idx);
        self.selected = None;
    }

    fn do_move(&mut self, mv: Option<Move>, node: GameNode) {
        self.last = mv.map(|m| m.to_vec().clone()).unwrap_or_default();
        self.clicked.clear();
        self.node = node;
        self.selected = None;
        self.select_idx = 0;
    }

    fn clear(&mut self, node: GameNode) {
        self.last.clear();
        self.clicked.clear();
        self.node = node;
        self.selected = None;
        self.select_idx = 0;
    }

    fn highlight(&self, idx: u32) -> bool {
        (self.clicked.is_empty() && self.selected.is_none() && self.last.contains(&idx))
            || self.clicked.contains(&idx)
    }

    fn suggest(&self, idx: u32) -> bool {
        !self.clicked.is_empty() && !self.clicked.contains(&idx)
    }

    fn animate(&self, idx: u32) -> bool {
        match self.selected {
            Some(mv) => mv.to_vec().contains(&idx),
            None => false,
        }
    }

    fn match_candidates(&self, node: &GameNode) -> Vec<Move> {
        match_candidates(node, &self.clicked)
    }

    fn try_select(&self) -> bool {
        !self.clicked.is_empty()
    }

    fn cycle(&mut self, reverse: bool) {
        let candidates = self.node.next();
        if candidates.is_empty() {
            return;
        }

        self.clicked.clear();

        match self.selected {
            None => {
                if reverse {
                    self.select_idx = candidates.len() - 1;
                } else {
                    self.select_idx = 0;
                }
            }
            Some(_) => {
                if reverse {
                    if self.select_idx == 0 {
                        self.select_idx = candidates.len() - 1;
                    } else {
                        self.select_idx -= 1;
                    }
                } else {
                    self.select_idx += 1;
                    if self.select_idx >= candidates.len() {
                        self.select_idx = 0;
                    }
                }
            }
        }
        self.selected = Some(candidates[self.select_idx]);
    }
}

fn white_count(node: &GameNode) -> u32 {
    node.counts().0.count_ones()
}

fn black_count(node: &GameNode) -> u32 {
    node.counts().1.count_ones()
}

#[derive(Eq, PartialEq, Clone, Copy)]
pub enum PlayerType {
    Human,
    Robot,
    Random,
}

impl PlayerType {
    fn get_icon(&self) -> &'static str {
        match &self {
            PlayerType::Human => HUMAN,
            PlayerType::Robot => ROBOT,
            PlayerType::Random => RANDOM,
        }
    }

    fn tooltip(&self) -> &'static str {
        match &self {
            PlayerType::Human => "Human",
            PlayerType::Robot => "AI",
            PlayerType::Random => "Random",
        }
    }
}
struct SetPlayer<'a> {
    player: &'a mut PlayerType,
    depth: &'a mut u32,
}

impl<'a> SetPlayer<'a> {
    fn new(player: &'a mut PlayerType, depth: &'a mut u32) -> SetPlayer<'a> {
        SetPlayer { player, depth }
    }
}

impl Widget for SetPlayer<'_> {
    fn ui(self, ui: &mut Ui) -> Response {
        ui.add(PlayerTypeToggle::new(self.player));

        ui.scope(|ui| {
            let active = match self.player {
                PlayerType::Human => false,
                PlayerType::Robot => true,
                PlayerType::Random => false,
            };

            ui.add_enabled(active, Slider::new(self.depth, 1..=12))
        })
        .inner
    }
}

struct PlayerTypeToggle<'a> {
    iter: std::iter::Cycle<std::slice::Iter<'static, PlayerType>>,
    target: &'a mut PlayerType,
}

impl PlayerTypeToggle<'_> {
    fn new(target: &mut PlayerType) -> PlayerTypeToggle<'_> {
        let mut iter = [PlayerType::Human, PlayerType::Robot, PlayerType::Random]
            .iter()
            .cycle();

        while iter.next().unwrap() != target {}

        PlayerTypeToggle { iter, target }
    }

    fn next(&mut self) -> PlayerType {
        *self.iter.next().unwrap()
    }
}

impl Widget for PlayerTypeToggle<'_> {
    fn ui(mut self, ui: &mut Ui) -> Response {
        let button = Button::new(self.target.get_icon());

        let size = Vec2::new(ui.spacing().interact_size.x, ui.spacing().interact_size.y);
        let r = ui
            .add_sized(size, button)
            .on_hover_text(self.target.tooltip());

        if r.clicked() {
            *self.target = self.next();
        }
        r
    }
}

fn piece_widget(piece: State) -> impl Widget {
    move |ui: &mut Ui| -> Response {
        let size = ui.spacing().interact_size.y;
        let piece = Piece::new(piece, Style::new(ui.style()));
        let (response, painter) = ui.allocate_painter(Vec2::splat(size), egui::Sense::hover());
        piece.draw(&response.rect, &painter);
        response
    }
}

pub struct NewPlayerInput {
    white: PlayerType,
    black: PlayerType,
    white_depth: u32,
    black_depth: u32,
}

const DEFAULT_AI_DEPTH: u32 = 7;

impl Default for NewPlayerInput {
    fn default() -> Self {
        Self::with(
            PlayerType::Human,
            PlayerType::Robot,
            DEFAULT_AI_DEPTH,
            DEFAULT_AI_DEPTH,
        )
    }
}

impl NewPlayerInput {
    pub fn with(
        white: PlayerType,
        black: PlayerType,
        white_depth: u32,
        black_depth: u32,
    ) -> NewPlayerInput {
        NewPlayerInput {
            white,
            black,
            white_depth,
            black_depth,
        }
    }
}

fn compute_xy(n: u32, reverse: bool) -> (u32, u32) {
    if reverse {
        let (x, y) = compute_xy(n, false);
        (9 - x, 9 - y)
    } else {
        let y = n / 5;
        let x = n - 5 * y;
        if y & 1 == 0 {
            (1 + x * 2, y)
        } else {
            (x * 2, y)
        }
    }
}

#[derive(Clone, Copy)]
struct Style {
    is_dark: bool,
}

impl Style {
    fn new(base: &style::Style) -> Style {
        let is_dark = base.visuals.dark_mode;
        Style { is_dark }
    }

    fn black_piece_color(&self) -> Color32 {
        Color32::BLACK
    }

    fn white_piece_color(&self) -> Color32 {
        Color32::WHITE
    }

    fn black_board_color(&self) -> Color32 {
        if self.is_dark {
            Color32::DARK_GRAY
        } else {
            Color32::from_rgb(112, 128, 144)
        }
    }

    fn white_board_color(&self) -> Color32 {
        if self.is_dark {
            Color32::LIGHT_GRAY
        } else {
            Color32::WHITE
        }
    }

    fn highligh_color(&self) -> Color32 {
        Color32::from_rgb(255, 165, 0)
    }
}

struct Piece {
    piece: dammen_core::game::State,
    style: Style,
}

impl Piece {
    fn new(piece: dammen_core::game::State, style: Style) -> Piece {
        Piece { piece, style }
    }

    fn draw_in_rect(&self, ui: &mut Ui) -> (Response, Painter) {
        let size = ui.spacing().interact_size.y * 2.0;

        let (response, painter) = ui.allocate_painter(Vec2::splat(size), egui::Sense::hover());
        self.draw(&response.rect, &painter);
        (response, painter)
    }

    fn draw(&self, rect: &Rect, painter: &Painter) {
        let base = |fill, stroke| {
            let s = Shape::circle_filled(rect.center(), rect.width() * 0.4, fill);
            painter.add(s);

            let stroke = Stroke::new(1.0, stroke);

            let s = Shape::circle_stroke(rect.center(), rect.width() * 0.4, stroke);
            painter.add(s);

            let s = Shape::circle_stroke(rect.center(), rect.width() * 0.35, stroke);
            painter.add(s);
        };

        let queen = |stroke| {
            let points = [
                Pos2::new(0.3, 0.7),
                Pos2::new(0.7, 0.7),
                Pos2::new(0.75, 0.32),
                Pos2::new(0.6, 0.5),
                Pos2::new(0.5, 0.32),
                Pos2::new(0.4, 0.5),
                Pos2::new(0.25, 0.32),
                Pos2::new(0.3, 0.7),
            ]
            .map(|p| (p * rect.width()) + rect.min.to_vec2());

            let stroke = Stroke::new(1.0, stroke);
            let s = Shape::line(points.to_vec(), stroke);
            painter.add(s);
        };

        match self.piece {
            State::Empty => {}
            State::White => {
                base(
                    self.style.white_piece_color(),
                    self.style.black_piece_color(),
                );
            }
            State::WhiteQueen => {
                base(
                    self.style.white_piece_color(),
                    self.style.black_piece_color(),
                );
                queen(self.style.black_piece_color());
            }
            State::Black => {
                base(
                    self.style.black_piece_color(),
                    self.style.white_piece_color(),
                );
            }
            State::BlackQueen => {
                base(
                    self.style.black_piece_color(),
                    self.style.white_piece_color(),
                );
                queen(self.style.white_piece_color());
            }
        };
    }
}

fn draw_board(ui: &mut Ui, node: &GameNode, board_size: f32, reversed: bool) {
    // todo: refactor and merge with DammenApp::draw_board
    let style = Style::new(ui.style());
    let square_size = board_size / 10.0;

    let (response, painter) =
        ui.allocate_painter(Vec2::new(board_size, board_size), Sense::hover());

    let to_screen = emath::RectTransform::from_to(
        Rect::from_min_size(Pos2::ZERO, response.rect.size()),
        response.rect,
    );

    let board_rect = Rect::from_min_max(Pos2::ZERO, Pos2::new(board_size, board_size));
    let board_rect = to_screen.transform_rect(board_rect);
    let board_rect = Shape::rect_filled(board_rect, Rounding::ZERO, style.white_board_color());
    painter.add(board_rect);

    for n in 1..=50 {
        let (x, y) = compute_xy(n - 1, reversed);

        let pos = Pos2::new(x as f32 * square_size, y as f32 * square_size);

        let field = Rect::from_min_max(pos, pos + Vec2::splat(square_size));
        let field = to_screen.transform_rect(field);
        painter.add(Shape::rect_filled(
            field,
            Rounding::ZERO,
            style.black_board_color(),
        ));

        Piece::new(node.at(n), style).draw(&field, &painter);

        painter.text(
            to_screen.transform_pos(pos + Vec2::new(square_size / 25.0, square_size)),
            Align2::LEFT_BOTTOM,
            format!("{}", n),
            FontId::proportional(square_size / 6.0),
            style.white_board_color(),
        );
    }
}

fn match_candidates(node: &GameNode, clicked: &[u32]) -> Vec<Move> {
    let legal_moves = node.next();
    legal_moves
        .iter()
        .filter(|m| {
            let as_v = m.to_vec();
            clicked.iter().all(|c| as_v.iter().any(|i| i == c))
        })
        .copied()
        .collect()
}

const REVERSE: &str = "🔄";
const NEW_GAME: &str = "⚔";
const CANCEL: &str = "🚫";
const SAVE_GAME: &str = "💾";
const LOAD_GAME: &str = "📂";
const ROBOT: &str = "🖳";
const HUMAN: &str = "👤";
const RANDOM: &str = "🎲";

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn coordinates() {
        let cases = [
            (0, (1, 0)),
            (1, (3, 0)),
            (2, (5, 0)),
            (3, (7, 0)),
            (4, (9, 0)),
            (5, (0, 1)),
            (9, (8, 1)),
            (10, (1, 2)),
            (44, (9, 8)),
            (45, (0, 9)),
            (49, (8, 9)),
        ];

        for (n, (ex, ey)) in cases {
            let (x, y) = compute_xy(n, false);
            assert_eq!(x, ex);
            assert_eq!(y, ey);
        }
    }

    #[test]
    fn coordinates_reversed() {
        let cases = [
            (0, (8, 9)),
            (1, (6, 9)),
            (2, (4, 9)),
            (3, (2, 9)),
            (4, (0, 9)),
            (5, (9, 8)),
            (9, (1, 8)),
            (10, (8, 7)),
            (44, (0, 1)),
            (45, (9, 0)),
            (49, (1, 0)),
        ];

        for (n, (ex, ey)) in cases {
            let (x, y) = compute_xy(n, true);
            assert_eq!(x, ex);
            assert_eq!(y, ey);
        }
    }

    #[test]
    fn match_candidates_test() {
        let string = "White 1
        | |B| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |w| |.| |.| |.| |.| |
        | |b| |b| |.| |.| |w|
        |.| |b| |.| |.| |.| |
        | |w| |.| |w| |.| |.|
        |w| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |W| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();
        let all_moves: Vec<Move> = game.next().iter().copied().collect();

        let candidates = match_candidates(&game, &[]);
        assert_eq!(all_moves, candidates);

        let candidates: Vec<_> = match_candidates(&game, &[33])
            .iter()
            .map(|m| m.to_vec())
            .collect();
        assert_eq!(vec![vec![29, 33], vec![28, 33]], candidates);

        let candidates: Vec<_> = match_candidates(&game, &[33, 28])
            .iter()
            .map(|m| m.to_vec())
            .collect();
        assert_eq!(vec![vec![28, 33]], candidates);

        let candidates: Vec<_> = match_candidates(&game, &[33, 28, 27])
            .iter()
            .map(|m| m.to_vec())
            .collect();
        assert!(candidates.is_empty());
    }

    #[test]
    fn input_state_starts_blank() {
        let is = InputState::new(GameNode::initial());

        assert!(is.selected.is_none());
        for n in 1..=50 {
            assert!(!is.highlight(n));
            assert!(!is.suggest(n));
            assert!(!is.animate(n));
        }
    }

    #[test]
    fn input_state_click_highlights_and_suggest_follow_up() {
        let mut is = InputState::new(GameNode::initial());
        is.click(33);

        assert!(is.highlight(33));
        assert!(is.suggest(28));
        assert!(is.suggest(29));
    }

    #[test]
    fn input_state_do_move_resets_and_highlights_last() {
        let mut is = InputState::new(GameNode::initial());
        is.click(33);

        let mv = is.node.next().as_slice()[0];
        is.do_move(Some(mv), is.node.apply(mv));

        assert!(is.selected.is_none());
        for n in 1..=50 {
            assert!(!is.suggest(n));
            assert!(!is.animate(n));
        }

        for n in mv.to_vec() {
            assert!(is.highlight(n));
        }
    }

    #[test]
    fn input_state_select_overrides_click() {
        let mut is = InputState::new(GameNode::initial());
        is.click(33);

        is.cycle(false);
        assert!(!is.highlight(33));
    }

    #[test]
    fn input_state_cycles() {
        let mut is = InputState::new(GameNode::initial());

        for mv in is.node.next().iter() {
            is.cycle(false);
            assert_eq!(*mv, is.selected.unwrap());
        }

        for mv in is.node.next().iter() {
            is.cycle(false);
            assert_eq!(*mv, is.selected.unwrap());
        }
    }

    #[test]
    fn input_state_cycles_in_reverse() {
        let mut is = InputState::new(GameNode::initial());
        let n = is.node.next();
        let mvs = n.as_slice();

        for mv in mvs.iter().rev() {
            is.cycle(true);
            assert_eq!(*mv, is.selected.unwrap());
        }

        for mv in mvs.iter().rev() {
            is.cycle(true);
            assert_eq!(*mv, is.selected.unwrap());
        }
    }

    #[test]
    fn input_state_last_is_reset_on_click() {
        let mut is = InputState::new(GameNode::initial());
        let mv = is.node.next().as_slice()[0];
        is.do_move(Some(mv), is.node.apply(mv));

        is.click(22);

        for n in mv.to_vec() {
            assert!(!is.highlight(n));
        }
    }

    #[test]
    fn input_state_last_is_reset_on_cycle() {
        let mut is = InputState::new(GameNode::initial());
        let mv = is.node.next().as_slice()[0];
        is.do_move(Some(mv), is.node.apply(mv));

        is.cycle(false);

        for n in mv.to_vec() {
            assert!(!is.highlight(n));
        }
    }
}
