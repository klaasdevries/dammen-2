#![warn(clippy::all, rust_2018_idioms)]
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

use clap::{Parser, ValueEnum};

#[derive(Clone, Copy, Debug, ValueEnum)]
enum PlayerType {
    /// Human (manual input)
    Human,
    /// AI
    AI,
    /// Random
    Random,
}

/// dammen-2
#[derive(Parser, Debug)]
struct Args {
    /// player type for the white player
    #[arg(short, long, value_enum, default_value = "human")]
    white_player: PlayerType,

    /// player type for the black player
    #[arg(short, long, value_enum, default_value = "ai")]
    black_player: PlayerType,

    /// load file containing a start position
    #[arg(short, long)]
    load: Option<String>,

    /// save game to this file afterwards
    #[arg(short, long)]
    save: Option<String>,

    /// depth of the ai
    #[arg(long, default_value = "7")]
    ai_depth: u32,

    /// enable debug logging
    #[arg(long)]
    debug: bool,
}

// When compiling natively:
#[cfg(not(target_arch = "wasm32"))]
fn main() -> std::result::Result<(), anyhow::Error> {
    use dammen_core::{game::GameNode, serialization::load_from_file};

    let args = Args::parse();

    let level = if args.debug {
        log::LevelFilter::Debug
    } else {
        log::LevelFilter::Info
    };
    pretty_env_logger::formatted_builder()
        .filter_level(level)
        .init();

    let start = match args.load {
        None => GameNode::initial(),
        Some(file) => load_from_file(&file)?,
    };

    let make_player = |typ: PlayerType| -> gui::PlayerType {
        match typ {
            PlayerType::Human => gui::PlayerType::Human,
            PlayerType::Random => gui::PlayerType::Random,
            PlayerType::AI => gui::PlayerType::Robot,
        }
    };

    let ai_depth = args.ai_depth;
    let white_player = make_player(args.white_player);
    let black_player = make_player(args.black_player);

    let players = gui::NewPlayerInput::with(white_player, black_player, ai_depth, ai_depth);

    let native_options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default()
            .with_maximized(true)
            .with_icon(load_icon()),
        // default_theme: fallback_theme(),
        // follow_system_theme: true,
        ..Default::default()
    };
    eframe::run_native(
        "Dammen 2",
        native_options,
        Box::new(move |_cc| Ok(Box::new(gui::DammenApp::with(start, players)))),
    )
    .map_err(|e| anyhow::Error::msg(e.to_string()))
}

#[cfg(not(target_arch = "wasm32"))]
fn load_icon() -> egui::viewport::IconData {
    const SIZE: u32 = 256;

    let icon_raw_data = include_bytes!("../assets/logo.svg");

    let opt = resvg::usvg::Options::default();
    let tree = resvg::usvg::Tree::from_data(icon_raw_data, &opt).expect("valid svg");
    let mut pixmap = resvg::tiny_skia::Pixmap::new(SIZE, SIZE).unwrap();
    let scale = SIZE as f32 / 100.0;
    resvg::render(
        &tree,
        resvg::tiny_skia::Transform::from_scale(scale, scale),
        &mut pixmap.as_mut(),
    );

    egui::viewport::IconData {
        rgba: pixmap.data().to_vec(),
        width: pixmap.width(),
        height: pixmap.height(),
    }
}

// When compiling to web using trunk:
#[cfg(target_arch = "wasm32")]
fn main() {
    use eframe::wasm_bindgen::JsCast as _;

    // Redirect `log` message to `console.log` and friends:
    eframe::WebLogger::init(log::LevelFilter::Debug).ok();

    let web_options = eframe::WebOptions::default();

    wasm_bindgen_futures::spawn_local(async {
        let document = eframe::web_sys::window()
            .expect("No window")
            .document()
            .expect("No document");

        let canvas = document
            .get_element_by_id("the_canvas_id")
            .expect("Failed to find the_canvas_id")
            .dyn_into::<eframe::web_sys::HtmlCanvasElement>()
            .expect("the_canvas_id was not a HtmlCanvasElement");

        if let Err(e) = eframe::WebRunner::new()
            .start(
                canvas,
                web_options,
                Box::new(|cc| Ok(Box::new(gui::DammenApp::new(cc)))),
            )
            .await
        {
            log::error!("{:?}", e);
        }
    });
}
