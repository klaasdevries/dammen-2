pub trait LoadFileTrait {
    fn open() -> Self;
    fn load(&mut self, ctx: &egui::Context) -> Option<Option<String>>;
}

pub trait SaveFileTrait {
    fn open(content: Vec<u8>) -> Self;
    fn save(&mut self, ctx: &egui::Context) -> Option<Result<(), std::io::Error>>;
}

#[cfg(not(target_arch = "wasm32"))]
mod native {
    use std::io::{self, Write};

    use super::*;

    pub struct LoadFileDialog {
        dialog: egui_file::FileDialog,
    }

    impl LoadFileTrait for LoadFileDialog {
        fn open() -> Self {
            let mut dialog = egui_file::FileDialog::open_file(None)
                .default_filename(DEFAULT_FILENAME)
                .filename_filter(Box::new(filename_filter))
                .show_files_filter(Box::new(filename_filter_path));
            dialog.open();

            LoadFileDialog { dialog }
        }

        fn load(&mut self, ctx: &egui::Context) -> Option<Option<String>> {
            if self.dialog.show(ctx).selected() {
                if let Some(file) = self.dialog.path() {
                    log::debug!("opening file {:?}", file);

                    return Some(std::fs::read_to_string(file).ok());
                }
            }
            None
        }
    }

    pub struct SaveFileDialog {
        dialog: egui_file::FileDialog,
        content: Vec<u8>,
    }

    impl SaveFileTrait for SaveFileDialog {
        fn open(content: Vec<u8>) -> Self {
            let mut dialog = egui_file::FileDialog::save_file(None)
                .default_filename(DEFAULT_FILENAME)
                .filename_filter(Box::new(filename_filter))
                .show_files_filter(Box::new(filename_filter_path));
            dialog.open();

            SaveFileDialog { dialog, content }
        }

        fn save(&mut self, ctx: &egui::Context) -> Option<Result<(), io::Error>> {
            if self.dialog.show(ctx).selected() {
                if let Some(file) = self.dialog.path() {
                    let do_write = || {
                        log::debug!("saving file {:?}", file);
                        let mut file = std::fs::File::create(file)?;
                        file.write_all(&self.content)?;
                        Ok(())
                    };

                    return Some(do_write());
                }
            }
            None
        }
    }

    fn filename_filter(filename: &str) -> bool {
        filename.ends_with(".yml") || filename.ends_with(".yaml")
    }

    fn filename_filter_path(file: &std::path::Path) -> bool {
        match file.to_str() {
            Some(s) => filename_filter(s),
            None => false,
        }
    }
}

#[cfg(target_arch = "wasm32")]
mod web {
    use super::*;
    use rfd::AsyncFileDialog;
    use wasm_bindgen_futures::spawn_local;

    pub struct LoadFileDialog {
        rx: tokio::sync::oneshot::Receiver<Option<String>>,
    }

    impl LoadFileTrait for LoadFileDialog {
        fn open() -> Self {
            let (tx, rx) = tokio::sync::oneshot::channel();

            spawn_local(async move {
                load_file(tx).await;
            });

            LoadFileDialog { rx }
        }

        fn load(&mut self, _ctx: &egui::Context) -> Option<Option<String>> {
            match self.rx.try_recv() {
                Ok(s) => Some(s),
                Err(_) => None,
            }
        }
    }

    pub struct SaveFileDialog {
        rx: tokio::sync::oneshot::Receiver<Option<Result<(), std::io::Error>>>,
    }

    impl SaveFileTrait for SaveFileDialog {
        fn open(content: Vec<u8>) -> Self {
            let (tx, rx) = tokio::sync::oneshot::channel();

            spawn_local(async move {
                save_file(content, tx).await;
            });

            SaveFileDialog { rx }
        }

        fn save(&mut self, _ctx: &egui::Context) -> Option<Result<(), std::io::Error>> {
            match self.rx.try_recv() {
                Ok(s) => s,
                Err(_) => None,
            }
        }
    }

    async fn load_file(tx: tokio::sync::oneshot::Sender<Option<String>>) {
        let file = AsyncFileDialog::new()
            .add_filter("dammen yaml", &["yaml", "yml"])
            .set_file_name(DEFAULT_FILENAME)
            .pick_file()
            .await;

        let s = match file {
            Some(f) => {
                let data = f.read().await;
                String::from_utf8(data).ok()
            }
            None => None,
        };

        if let Err(e) = tx.send(s) {
            log::error!("sending {:?}", e);
        }
    }

    async fn save_file(
        content: Vec<u8>,
        tx: tokio::sync::oneshot::Sender<Option<Result<(), std::io::Error>>>,
    ) {
        let file = AsyncFileDialog::new()
            .set_file_name(DEFAULT_FILENAME)
            .save_file()
            .await;

        let s = match file {
            Some(f) => Some(f.write(&content).await),
            None => None,
        };

        if let Err(e) = tx.send(s) {
            log::error!("sending {:?}", e);
        }
    }
}

#[cfg(not(target_arch = "wasm32"))]
pub use native::LoadFileDialog;
#[cfg(not(target_arch = "wasm32"))]
pub use native::SaveFileDialog;
#[cfg(target_arch = "wasm32")]
pub use web::LoadFileDialog;
#[cfg(target_arch = "wasm32")]
pub use web::SaveFileDialog;

const DEFAULT_FILENAME: &str = "dammen-2.yaml";
