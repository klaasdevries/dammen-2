use ai::AI;
use dammen_core::game::{GameNode, Move};

pub trait AIExecutorTrait {
    fn new() -> Self;
    fn is_blocking(&self) -> bool;
    fn is_working(&self) -> bool;
    fn poll(&mut self) -> Option<Option<Move>>;
    fn dispatch(&mut self, depth: u32, game: GameNode) -> bool;
}

#[cfg(not(target_arch = "wasm32"))]
mod native {
    use super::*;
    pub struct AIExecutor {
        tx: Option<std::sync::mpsc::SyncSender<(u32, GameNode)>>,
        rx: std::sync::mpsc::Receiver<Option<Move>>,
        join: Option<std::thread::JoinHandle<()>>,
        working: bool,
    }

    impl AIExecutorTrait for AIExecutor {
        fn new() -> AIExecutor {
            let (tx_in, rx_in) = std::sync::mpsc::sync_channel::<(u32, GameNode)>(1);

            let (tx_out, rx_out) = std::sync::mpsc::sync_channel(1);

            let join = std::thread::spawn(move || {
                while let Ok((depth, node)) = rx_in.recv() {
                    let mut algo = ai::MinimaxAI::new(depth);
                    let mv = algo.best_move(&node);
                    if let Err(e) = tx_out.send(mv) {
                        log::error!("{}", e);
                        break;
                    }
                }
            });

            AIExecutor {
                tx: Some(tx_in),
                rx: rx_out,
                join: Some(join),
                working: false,
            }
        }

        fn is_blocking(&self) -> bool {
            false
        }

        fn is_working(&self) -> bool {
            self.working
        }

        fn poll(&mut self) -> Option<Option<Move>> {
            match self.rx.try_recv().ok() {
                Some(mv) => {
                    self.working = false;
                    Some(mv)
                }
                None => None,
            }
        }

        fn dispatch(&mut self, depth: u32, game: GameNode) -> bool {
            if self.working {
                return false;
            }

            match &self.tx {
                Some(tx) => {
                    use std::sync::mpsc::TrySendError;
                    match tx.try_send((depth, game)) {
                        Ok(()) => {
                            self.working = true;
                            true
                        }
                        Err(TrySendError::Full(_)) => false,
                        Err(TrySendError::Disconnected(_)) => {
                            log::error!("disconnected");
                            false
                        }
                    }
                }
                None => {
                    log::error!("dropped");
                    false
                }
            }
        }
    }

    impl Drop for AIExecutor {
        fn drop(&mut self) {
            drop(self.tx.take());
            self.join.take().map(|j| j.join());
        }
    }
}

#[cfg(target_arch = "wasm32")]
mod web {
    use super::*;

    pub struct AIExecutor {
        item: Option<(u32, GameNode)>,
    }

    #[cfg(target_arch = "wasm32")]
    impl AIExecutorTrait for AIExecutor {
        fn new() -> AIExecutor {
            AIExecutor { item: None }
        }

        fn is_blocking(&self) -> bool {
            true
        }

        fn is_working(&self) -> bool {
            self.item.is_some()
        }

        fn poll(&mut self) -> Option<Option<Move>> {
            match self.item.take() {
                Some((depth, node)) => {
                    let mut algo = ai::MinimaxAI::new(depth);
                    let mv = algo.best_move(&node);
                    Some(mv)
                }
                None => None,
            }
        }

        fn dispatch(&mut self, depth: u32, game: GameNode) -> bool {
            if self.is_working() {
                return false;
            }

            self.item = Some((depth, game));
            true
        }
    }
}

#[cfg(not(target_arch = "wasm32"))]
pub use native::AIExecutor;
#[cfg(target_arch = "wasm32")]
pub use web::AIExecutor;

pub struct AIPlayer {
    ai_depth: u32,
}

impl AIPlayer {
    pub fn new(depth: u32) -> AIPlayer {
        AIPlayer { ai_depth: depth }
    }

    pub fn depth(&self) -> u32 {
        self.ai_depth
    }
}

pub enum Player {
    Human,
    Robot(AIPlayer),
    Random(ai::RandomAI),
}

pub struct MoveTx {
    tx: std::rc::Rc<std::cell::RefCell<Option<Move>>>,
}

impl MoveTx {
    pub fn set(&mut self, mv: Option<Move>) {
        self.tx.replace(mv);
    }
}

pub struct MoveRx {
    rx: std::rc::Rc<std::cell::RefCell<Option<Move>>>,
}

impl dammen_core::manager::Player for MoveRx {
    fn choose(&mut self, _game: &GameNode, _ctx: &dammen_core::manager::Context) -> Option<Move> {
        self.rx.take()
    }
}

// core::manager::GameManager has a bit of an unlucky interface where choose()
// expects full control, while in a gui we will want to remain responsive.
//
// The move_channel() is a (maybe not very elegant) workaround to set the next
// move before calling choose(), so it won't block
pub fn move_channel() -> (MoveTx, MoveRx) {
    let tx = std::rc::Rc::new(std::cell::RefCell::new(None));

    let rx = tx.clone();

    (MoveTx { tx }, MoveRx { rx })
}

#[cfg(test)]
mod tests {
    use dammen_core::manager::Player;

    use super::*;

    #[test]
    fn async_ai_executor() {
        let string = "White 1
        | |B| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |w| |.| |.| |.| |.| |
        | |b| |b| |.| |.| |w|
        |.| |b| |.| |.| |.| |
        | |w| |.| |w| |.| |.|
        |w| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |W| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();

        let mut executor = AIExecutor::new();
        assert!(!executor.is_working());

        assert!(executor.dispatch(3, game));
        assert!(executor.is_working());
        assert!(!executor.dispatch(3, game));
        assert!(executor.is_working());

        loop {
            if let Some(mv) = executor.poll() {
                assert!(!executor.is_working());
                assert!(mv.is_some());
                break;
            }
        }
    }

    #[test]
    fn test_move_channel() {
        let (mut tx, mut rx) = move_channel();

        let node = GameNode::initial();
        let ctx = dammen_core::manager::Context { log: &[] };

        assert!(rx.choose(&node, &ctx).is_none());

        let mv = node.next().iter().copied().next();
        assert!(mv.is_some());
        tx.set(mv);

        assert_eq!(mv, rx.choose(&node, &ctx));
    }
}
