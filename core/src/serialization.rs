use crate::game;
use regex;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use game::{GameNode, RichMoves, Side, State};

use crate::Error;
use crate::Result;

#[derive(Deserialize, Serialize, Debug)]
pub struct Game {
    #[serde(default = "default_initial_board")]
    pub board: HashMap<u32, State>,
    #[serde(default = "default_side_to_move")]
    pub side_to_move: Side,
    #[serde(default = "default_move_number")]
    pub move_number: u32,
    #[serde(default)]
    pub moves_before: Vec<String>,
    #[serde(default)]
    pub moves_after: Vec<String>,
}

#[derive(Deserialize, Serialize, Debug)]
struct GameHolder {
    game: Game,
}

fn default_initial_board() -> HashMap<u32, State> {
    let init = game::GameNode::initial();
    extract_board(init)
}

fn default_side_to_move() -> Side {
    Side::White
}

fn default_move_number() -> u32 {
    1
}

pub fn load_from_file(filename: &str) -> Result<GameNode> {
    let raw_data = std::fs::read_to_string(filename)?;
    load_from_string(raw_data.as_str())
}

pub fn load_from_string(raw_data: &str) -> Result<GameNode> {
    let gameholder: GameHolder = serde_yaml::from_str(raw_data)
        .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, format!("{}", e)))?;

    let gamerep = gameholder.game;
    construct_node(gamerep)
}

pub fn construct_node(gamerep: Game) -> Result<GameNode> {
    let builder = || {
        let mut b = game::GameNodeBuilder::empty();
        for (idx, piece) in gamerep.board.into_iter() {
            b = b.with_piece(idx, piece);
        }
        b
    };

    let expect = builder()
        .with_move(gamerep.move_number, gamerep.side_to_move)
        .build();

    let game = if gamerep.moves_before.is_empty() {
        expect
    } else {
        let game = apply_moves(GameNode::initial(), gamerep.moves_before)?;

        if game != expect {
            return Err(Error::msg(format!(
                "sanity check failed. After applying moves the board should be {}, but is {}",
                expect, game
            )));
        }
        game
    };

    let game = apply_moves(game, gamerep.moves_after)?;

    Ok(game)
}

fn apply_moves(mut game: GameNode, moves: Vec<String>) -> Result<GameNode> {
    for m in moves {
        game = apply_move(game, &m)?;
    }
    Ok(game)
}

pub fn apply_move(game: GameNode, mv: &str) -> Result<GameNode> {
    let candidates = find_matching_moves(game, mv);
    let game = match candidates.len() {
        0 => return Err(Error::msg("no valid moves")),
        1 => game.apply_rich(candidates[0].clone()),
        _ => return Err(Error::msg(format!("ambigious move: {}", mv))),
    };
    Ok(game)
}

pub fn find_matching_moves(game: GameNode, mv: &str) -> RichMoves {
    let candidates = game.rich_next();
    let moves_vec = extract_moves(mv);

    candidates
        .iter()
        .filter(|m| {
            let as_v = m.to_vec();
            moves_vec.iter().all(|i| as_v.iter().any(|j| *i == *j))
        })
        .cloned()
        .collect()
}

fn extract_moves(moverep: &str) -> Vec<u32> {
    use std::sync::OnceLock;

    static MOVE_RE: OnceLock<regex::Regex> = OnceLock::new();
    let move_re = MOVE_RE.get_or_init(|| regex::Regex::new(r"([0-9]+)").unwrap());

    move_re
        .find_iter(moverep)
        .map(|m| m.as_str())
        .map(|s: &str| s.parse::<u32>().unwrap())
        .collect()
}

pub fn dump(
    writer: &mut dyn std::io::Write,
    game: GameNode,
    moves_before: &[String],
    moves_after: &[String],
) -> Result<()> {
    let board = extract_board(game);

    let (move_number, side_to_move) = game.movenumber();
    let gameholder = GameHolder {
        game: Game {
            board,
            side_to_move,
            move_number,
            moves_before: moves_before.to_vec(),
            moves_after: moves_after.to_vec(),
        },
    };

    serde_yaml::to_writer(writer, &gameholder).map_err(|e| Error::msg(e.to_string()))
}

pub fn extract_board(game: game::GameNode) -> HashMap<u32, State> {
    (1..51)
        .map(|idx| {
            let piece = game.at(idx);
            (idx, piece)
        })
        .filter(|(_, piece)| *piece != State::Empty)
        .collect()
}
