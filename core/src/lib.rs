pub type Result<T> = anyhow::Result<T>;
use anyhow::Error;

#[macro_use]
pub mod bittwiddling;
pub mod game;
pub mod manager;
pub mod serialization;
