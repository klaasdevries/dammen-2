use crate::{game, serialization};

pub use game::Winner;
use game::{GameNode, Move, Side};

use crate::Error;
use crate::Result;

pub struct Context<'a> {
    pub log: &'a [String],
}

pub trait Player {
    fn choose(&mut self, game: &GameNode, ctx: &Context) -> Option<Move>;
}

pub struct GameManager {
    game: GameNode,
    white_player: Box<dyn Player>,
    black_player: Box<dyn Player>,
    log: Vec<String>,
}

impl GameManager {
    pub fn new(white: Box<dyn Player>, black: Box<dyn Player>) -> GameManager {
        GameManager::with_initial(white, black, GameNode::initial())
    }

    pub fn with_initial(
        white: Box<dyn Player>,
        black: Box<dyn Player>,
        game: GameNode,
    ) -> GameManager {
        GameManager {
            game,
            white_player: white,
            black_player: black,
            log: Vec::new(),
        }
    }

    pub fn get_game(&self) -> &GameNode {
        &self.game
    }

    pub fn get_log(&self) -> &[String] {
        self.log.as_slice()
    }

    pub fn advance(&mut self) -> Option<Winner> {
        if let Some(w) = self.game.winner() {
            return Some(w);
        }

        let ctx = Context {
            log: self.log.as_slice(),
        };

        let (mv, s) = match self.game.movenumber().1 {
            Side::White => (self.white_player.choose(&self.game, &ctx), Side::Black),
            Side::Black => (self.black_player.choose(&self.game, &ctx), Side::White),
        };
        if mv.is_none() {
            return Some(Winner::from_side(s));
        }
        let mv = mv.unwrap();

        let rm = self.game.find_matching_rich(mv);

        self.game = self.game.apply(mv);
        self.log.push(rm.to_string());
        self.game.winner()
    }

    pub fn play(&mut self) -> Winner {
        loop {
            let w = self.advance();
            if let Some(w) = w {
                return w;
            }
        }
    }

    pub fn save(&self, writer: &mut dyn std::io::Write) -> Result<()> {
        serialization::dump(writer, self.game, self.log.as_slice(), &[])
            .map_err(|e| Error::msg(e.to_string()))
    }
}
