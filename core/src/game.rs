use serde::{Deserialize, Serialize};
use smallvec::{smallvec, SmallVec};
use std::fmt;

use crate::bittwiddling::*;

use crate::Error;
use crate::Result;

#[derive(Serialize, Deserialize, Debug, Clone, Copy, Eq, PartialEq)]
pub enum State {
    Empty,
    White,
    Black,
    WhiteQueen,
    BlackQueen,
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy, Eq, PartialEq)]
pub enum Side {
    White,
    Black,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Winner {
    White,
    Black,
    Draw(&'static str),
}

impl Winner {
    pub fn from_side(s: Side) -> Winner {
        match s {
            Side::White => Winner::White,
            Side::Black => Winner::Black,
        }
    }
}

#[derive(Clone, Copy, Eq, PartialEq, Debug)]
pub struct GameState {
    white_mask: u64, // bits 50-64 store move number
    black_mask: u64, // bits 50-64 store the move number with the last piece move or capture
    queen_mask: u64,
}
struct Expand {
    foes: u64,
    empty: u64,
}

pub trait BaseMove {
    fn new(src: u64, dst: u64) -> Self;
    fn cap(src: u64, cap: u64, dst: u64) -> Self;
    fn expand(&self, other: Self) -> Self;
    fn num_removed(&self) -> u32;
}

impl GameState {
    fn initial() -> GameState {
        let game = GameState {
            white_mask: 0x3ffffc0000000,
            black_mask: 0xfffff,
            queen_mask: 0,
        };

        assert!(
            field_mask!(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
                == game.black_mask
        );
        assert!(
            field_mask!(
                30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49
            ) == game.white_mask
        );

        game
    }

    // Gets a game representation from a string, mostly useful for setting up test cases
    fn from_string(s: &str) -> Result<GameState> {
        use std::sync::OnceLock;

        static REGEX: OnceLock<(regex::Regex, regex::Regex)> = OnceLock::new();
        let (header_re, row_re) = REGEX.get_or_init(|| {
            let h = regex::Regex::new(r"(?P<side>White|Black)\s+(?P<move>[0-9]+)").unwrap();
            let r = regex::Regex::new(
                r"(\s|\|)*(.|w|b|W|B)\| \|(.|w|b|W|B)\| \|(.|w|b|W|B)\| \|(.|w|b|W|B)\| \|(.|w|b|W|B)(\s|\|)*").unwrap();
                (h, r)
        });

        let mut game = GameState {
            white_mask: 0,
            black_mask: 0,
            queen_mask: 0,
        };
        let mut lines = s.lines();
        let header = lines.next().ok_or_else(|| Error::msg("no header line"))?;
        let header = header_re
            .captures(header)
            .ok_or_else(|| Error::msg("mismatched header"))?;

        let movenumber: u32 = header.name("move").unwrap().as_str().parse().unwrap();
        let side: Side = match header.name("side").unwrap().as_str() {
            "White" => Side::White,
            "Black" => Side::Black,
            _ => panic!("bad side"),
        };
        let raw_movenumber: u32 = match side {
            Side::White => (movenumber - 1) << 1,
            Side::Black => ((movenumber - 1) << 1) + 1,
        };
        GameState::set_squeezed_in(raw_movenumber, &mut game.white_mask);
        game.mark_regulatory_draw();

        lines.take(10).enumerate().try_for_each(|(row_idx, line)| {
            let captures = match row_re.captures(line) {
                Some(c) => c,
                None => return Err(Error::msg("mismatched row")),
            };

            let start_idx = row_idx * 5;
            let s = |c: regex::Match| match c.as_str() {
                "." => State::Empty,
                "w" => State::White,
                "b" => State::Black,
                "W" => State::WhiteQueen,
                "B" => State::BlackQueen,
                _ => panic!("unknown char"),
            };

            for col_idx in 1..6 {
                let c = captures.get(col_idx + 1).unwrap();
                game.set(as_field_idx(start_idx + (col_idx - 1)), s(c));
            }
            Ok(())
        })?;

        Ok(game)
    }

    fn raw_movenumber(&self) -> u32 {
        GameState::get_squeezed_in(self.white_mask)
    }

    fn movenumber(&self) -> u32 {
        self.raw_movenumber() >> 1
    }

    fn side_to_move(&self) -> Side {
        match (self.raw_movenumber() & 1) == 0 {
            true => Side::White,
            false => Side::Black,
        }
    }

    fn is_regulatory_draw(&self) -> bool {
        let last_piece_move_or_capture = GameState::get_squeezed_in(self.black_mask);
        self.raw_movenumber() - last_piece_move_or_capture >= 50
    }

    fn mark_regulatory_draw(&mut self) {
        GameState::set_squeezed_in(self.raw_movenumber(), &mut self.black_mask);
    }

    fn advance(&mut self) {
        let n = self.raw_movenumber() + 1;
        GameState::set_squeezed_in(n, &mut self.white_mask);
    }

    fn get(&self, idx: u64) -> State {
        assert_field_mask!(idx);

        let is_queen = self.is_queen(idx);
        if self.is_white(idx) {
            if is_queen {
                State::WhiteQueen
            } else {
                State::White
            }
        } else if self.is_black(idx) {
            if is_queen {
                State::BlackQueen
            } else {
                State::Black
            }
        } else {
            State::Empty
        }
    }

    fn apply(&mut self, move_mask: u64) {
        assert_field_mask!(move_mask);
        let friend = match self.side_to_move() {
            Side::White => self.white_mask,
            Side::Black => self.black_mask,
        };

        let src_bit = friend & move_mask;
        let dst_bit = self.empty() & move_mask;
        let is_queen_move = (src_bit & self.queen_mask) != 0;
        let is_capture_move = move_mask.count_ones() > 2;

        assert!(src_bit.count_ones() == 1);
        assert!(dst_bit.count_ones() == 1);

        let flip = |mut b| {
            if (b & src_bit) != 0 {
                b |= dst_bit;
            }
            b
        };

        self.white_mask = flip(self.white_mask);
        self.black_mask = flip(self.black_mask);
        self.queen_mask = flip(self.queen_mask);

        let erase_mask = !(move_mask & !dst_bit);

        self.white_mask &= erase_mask;
        self.black_mask &= erase_mask;
        self.queen_mask &= erase_mask;

        if (self.side_to_move() == Side::White && (dst_bit & NORTH_ROW) != 0)
            || (self.side_to_move() == Side::Black && (dst_bit & SOUTH_ROW) != 0)
        {
            self.queen_mask |= dst_bit
        }

        self.advance();
        if !is_queen_move || is_capture_move {
            self.mark_regulatory_draw();
        }
    }

    fn set(&mut self, fieldmask: u64, state: State) {
        assert_field_mask!(fieldmask);

        match state {
            State::White => {
                self.white_mask |= fieldmask;
                self.black_mask &= !fieldmask;
                self.queen_mask &= !fieldmask;
            }
            State::Black => {
                self.white_mask &= !fieldmask;
                self.black_mask |= fieldmask;
                self.queen_mask &= !fieldmask;
            }
            State::WhiteQueen => {
                self.white_mask |= fieldmask;
                self.black_mask &= !fieldmask;
                self.queen_mask |= fieldmask;
            }
            State::BlackQueen => {
                self.white_mask &= !fieldmask;
                self.black_mask |= fieldmask;
                self.queen_mask |= fieldmask;
            }
            State::Empty => {
                self.white_mask &= !fieldmask;
                self.black_mask &= !fieldmask;
                self.queen_mask &= !fieldmask;
            }
        };
    }

    fn find_moves<M: BaseMove + Clone>(&self) -> Moves<M> {
        let mut mvs = Moves::new();

        if !self.is_regulatory_draw() {
            match self.side_to_move() {
                Side::White => self.find_white_moves(&mut mvs),
                Side::Black => self.find_black_moves(&mut mvs),
            }
        }
        mvs
    }

    fn find_white_moves<M: BaseMove + Clone>(&self, mvs: &mut Moves<M>) {
        debug_assert!(self.side_to_move() == Side::White);

        let white = self.white_mask & BOARD_MASK & !(self.queen_mask);
        let empty = self.empty();

        GameState::find_non_regular_moves(
            mvs,
            self.white_mask & BOARD_MASK,
            self.black_mask & BOARD_MASK,
            self.queen_mask & BOARD_MASK,
        );

        if mvs.num_removed() == 0 {
            [Direction::NE, Direction::NW]
                .iter()
                .flat_map(move |dir| {
                    let move_to = dir_off(*dir, white) & empty;

                    FieldMaskIter::new(move_to)
                        .map(move |dst| (dir_off(counter_dir(*dir), dst), dst))
                        .map(|(src, dst)| M::new(src, dst))
                })
                .for_each(|m| mvs.add(m));
        }
    }

    fn find_black_moves<M: BaseMove + Clone>(&self, mvs: &mut Moves<M>) {
        debug_assert!(self.side_to_move() == Side::Black);

        let black = (self.black_mask & BOARD_MASK) & !(self.queen_mask);
        let empty = self.empty();

        GameState::find_non_regular_moves(mvs, self.black_mask, self.white_mask, self.queen_mask);

        if mvs.num_removed() == 0 {
            [Direction::SE, Direction::SW]
                .iter()
                .flat_map(move |dir| {
                    let move_to = dir_off(*dir, black) & empty;

                    FieldMaskIter::new(move_to)
                        .map(move |dst| (dir_off(counter_dir(*dir), dst), dst))
                        .map(|(src, dst)| M::new(src, dst))
                })
                .for_each(|m| mvs.add(m));
        }
    }

    fn find_non_regular_moves<M: BaseMove + Clone>(
        mvs: &mut Moves<M>,
        friends: u64,
        foes: u64,
        queens: u64,
    ) {
        let expand = Expand {
            foes,
            empty: !(friends | foes),
        };
        GameState::find_queen_moves(mvs, friends, &expand, queens);
        GameState::find_piece_captures(mvs, friends & !queens, &expand, 0);
    }

    fn find_queen_moves<M: BaseMove + Clone>(
        mvs: &mut Moves<M>,
        friends: u64,
        expand: &Expand,
        queens: u64,
    ) {
        let friend_queens = friends & queens;
        for start in FieldMaskIter::new(friend_queens) {
            for dir in &[Direction::NE, Direction::NW, Direction::SE, Direction::SW] {
                for dst in
                    FieldMaskIter::new(dir_line(start, *dir)).filter(|d| *d & expand.empty != 0)
                {
                    let span = line(from_field_idx(start), from_field_idx(dst));
                    let cap = expand.foes & span;
                    let total = (span & BOARD_MASK).count_ones();
                    let foes = cap.count_ones();
                    let empty = (expand.empty & span).count_ones();

                    if empty == total {
                        mvs.add(M::new(start, dst));
                    } else if foes == 1 && empty + 1 == total {
                        let mv = M::cap(start, cap, dst);
                        mvs.add(mv.clone());

                        let mut es = Moves::new();
                        GameState::find_queen_moves_expand(&mut es, dst, expand, cap, *dir);

                        mvs.expand(&mv, es);
                    }
                }
            }
        }
    }

    fn find_queen_moves_expand<M: BaseMove + Clone>(
        mvs: &mut Moves<M>,
        start: u64,
        expand: &Expand,
        protected: u64,
        dir: Direction,
    ) {
        for other_dir in [Direction::NE, Direction::NW, Direction::SE, Direction::SW]
            .iter()
            .copied()
            .filter(|d| *d != counter_dir(dir))
        {
            for dst in
                FieldMaskIter::new(dir_line(start, other_dir)).filter(|d| *d & expand.empty != 0)
            {
                let span = line(from_field_idx(start), from_field_idx(dst));
                let cap = expand.foes & !protected & span;
                let total = (span & BOARD_MASK).count_ones();
                let foes = cap.count_ones();
                let empty = (expand.empty & span).count_ones();

                if foes == 1 && empty + 1 == total {
                    let mv = M::cap(start, cap, dst);
                    mvs.add(mv.clone());

                    let mut es = Moves::new();
                    GameState::find_queen_moves_expand(
                        &mut es,
                        dst,
                        expand,
                        protected | cap,
                        other_dir,
                    );

                    mvs.expand(&mv, es);
                }
            }
        }
    }

    fn find_piece_captures<M: BaseMove + Clone>(
        mvs: &mut Moves<M>,
        start: u64,
        expand: &Expand,
        protected: u64,
    ) {
        // captures
        for dir in &[Direction::NE, Direction::NW, Direction::SE, Direction::SW] {
            if let Some((_, move_to)) =
                GameState::try_capture(start, expand.empty, expand.foes & !protected, *dir)
            {
                FieldMaskIter::new(move_to)
                    .map(move |dst| {
                        // todo: handle multi capture
                        let c = dir_off(counter_dir(*dir), dst);
                        let s = dir_off(counter_dir(*dir), c);
                        (s, c, dst)
                    })
                    .for_each(|(s, c, dst)| {
                        let mv = M::cap(s, c, dst);
                        mvs.add(mv.clone());

                        // todo: fight the compiler. There should be a way to do this without expanding into a vector
                        let mut es = Moves::new();
                        GameState::find_piece_captures(&mut es, dst, expand, protected | c);

                        mvs.expand(&mv, es);
                    });
            }
        }
    }

    #[inline(always)]
    fn try_move(from: u64, empty: u64, dir: Direction) -> Option<u64> {
        match dir_off(dir, from) & empty {
            0 => None,
            n => Some(n),
        }
    }

    #[inline(always)]
    fn try_capture(from: u64, empty: u64, foes: u64, dir: Direction) -> Option<(u64, u64)> {
        GameState::try_move(from, foes, dir)
            .and_then(|c| GameState::try_move(c, empty, dir).map(|d| (c, d)))
    }

    #[inline(always)]
    fn is_white(&self, idx: u64) -> bool {
        (self.white_mask & idx) != 0
    }

    #[inline(always)]
    fn is_black(&self, idx: u64) -> bool {
        (self.black_mask & idx) != 0
    }

    #[inline(always)]
    fn is_queen(&self, idx: u64) -> bool {
        (self.queen_mask & idx) != 0
    }

    #[inline(always)]
    fn empty(&self) -> u64 {
        (!(self.white_mask | self.black_mask)) & BOARD_MASK
    }

    fn set_squeezed_in(n: u32, on: &mut u64) {
        debug_assert!(n < (1 << 14));
        *on &= !((0x7ff_u64) << 50);
        *on |= (n as u64) << 50;
    }

    fn get_squeezed_in(on: u64) -> u32 {
        (on >> 50) as u32
    }
}

impl fmt::Display for GameState {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "{:?}   {:2}", self.side_to_move(), self.movenumber() + 1)?;

        let as_char = |s: State| match s {
            State::Empty => '.',
            State::White => 'w',
            State::Black => 'b',
            State::WhiteQueen => 'W',
            State::BlackQueen => 'B',
        };

        for row in 0..10 {
            let start = row * 5;
            if (row & 1) == 0 {
                writeln!(
                    f,
                    "| |{}| |{}| |{}| |{}| |{}|  {:2} - {:2}",
                    as_char(self.get(as_field_idx(start))),
                    as_char(self.get(as_field_idx(start + 1))),
                    as_char(self.get(as_field_idx(start + 2))),
                    as_char(self.get(as_field_idx(start + 3))),
                    as_char(self.get(as_field_idx(start + 4))),
                    start + 1,
                    start + 5
                )?;
            } else {
                writeln!(
                    f,
                    "|{}| |{}| |{}| |{}| |{}| |  {:2} - {:2}",
                    as_char(self.get(as_field_idx(start))),
                    as_char(self.get(as_field_idx(start + 1))),
                    as_char(self.get(as_field_idx(start + 2))),
                    as_char(self.get(as_field_idx(start + 3))),
                    as_char(self.get(as_field_idx(start + 4))),
                    start + 1,
                    start + 5
                )?;
            };
        }

        Ok(())
    }
}

#[derive(Clone, Copy, Eq, PartialEq)]
pub struct Move {
    // sets the bit for all affected fields.
    // the src field will go from non-empty to empty, the dst field from empty to non-empty
    // all others set will be removed
    mask: u64,
}

impl BaseMove for Move {
    fn new(src: u64, dst: u64) -> Move {
        let m = src | dst;
        assert_field_mask!(m);
        Move {
            mask: m & BOARD_MASK,
        }
    }

    fn cap(src: u64, cap: u64, dst: u64) -> Move {
        let m = src | cap | dst;
        assert_field_mask!(m);
        Move {
            mask: m & BOARD_MASK,
        }
    }

    fn expand(&self, other: Move) -> Move {
        Move {
            mask: self.mask ^ other.mask,
        }
    }

    fn num_removed(&self) -> u32 {
        let num = self.mask.count_ones();
        assert!(num >= 2);
        num - 2
    }
}

impl Move {
    pub fn to_vec(self) -> Vec<u32> {
        to_field_vec(self.mask).iter().map(|i| i + 1).collect()
    }
}

impl std::fmt::Debug for Move {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", to_field_vec(self.mask))
    }
}

#[derive(Clone)]
pub struct RichMove {
    base: Move,
    visited: SmallVec<[u8; 4]>,
}

impl BaseMove for RichMove {
    fn new(src: u64, dst: u64) -> RichMove {
        RichMove {
            base: Move::new(src, dst),
            visited: smallvec!(from_field_idx(src), from_field_idx(dst)),
        }
    }

    fn cap(src: u64, cap: u64, dst: u64) -> RichMove {
        RichMove {
            base: Move::cap(src, cap, dst),
            visited: smallvec!(from_field_idx(src), from_field_idx(dst)),
        }
    }

    fn expand(&self, other: RichMove) -> RichMove {
        let mut v = self.visited.clone();
        if v.last() == other.visited.first() {
            v.pop();
        }
        v.extend(other.visited);

        RichMove {
            base: self.base.expand(other.base),
            visited: v,
        }
    }

    fn num_removed(&self) -> u32 {
        self.base.num_removed()
    }
}

impl RichMove {
    pub fn to_vec(&self) -> Vec<u32> {
        self.visited.iter().map(|i| (i + 1) as u32).collect()
    }

    pub fn simple(&self) -> Move {
        self.base
    }
}

impl std::fmt::Display for RichMove {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let visited: Vec<String> = self.to_vec().iter().map(|i| i.to_string()).collect();
        let visited = if self.num_removed() == 0 {
            visited.join(" - ")
        } else {
            visited.join(" x ")
        };
        write!(f, "{}", visited)
    }
}

type MovesImpl<M> = SmallVec<[M; 8]>;
pub struct Moves<M: BaseMove = Move>(MovesImpl<M>);

impl<M: BaseMove> Moves<M> {
    fn new() -> Moves<M> {
        Moves(MovesImpl::new())
    }

    fn add(&mut self, mv: M) {
        if self.is_empty() || mv.num_removed() == self.0[0].num_removed() {
            self.0.push(mv);
        } else if mv.num_removed() > self.0[0].num_removed() {
            self.0.clear();
            self.0.push(mv);
        }
    }

    fn num_removed(&self) -> u32 {
        if self.is_empty() {
            0
        } else {
            self.0[0].num_removed()
        }
    }

    fn expand(&mut self, base: &M, other: Self) {
        for e in other.0 {
            self.add(base.expand(e));
        }
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    pub fn iter(&self) -> impl Iterator<Item = &M> {
        self.0.iter()
    }

    pub fn as_slice(&self) -> &[M] {
        self.0.as_slice()
    }
}

impl<M: BaseMove> std::ops::Index<usize> for Moves<M> {
    type Output = M;

    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

impl<M: BaseMove> std::iter::FromIterator<M> for Moves<M> {
    fn from_iter<I: IntoIterator<Item = M>>(iter: I) -> Self {
        let mut mvs = Moves::new();
        for m in iter {
            mvs.add(m);
        }
        mvs
    }
}

pub type RichMoves = Moves<RichMove>;

impl std::fmt::Display for RichMoves {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for mv in self.0.iter() {
            writeln!(f, "{}", mv)?;
        }
        Ok(())
    }
}

#[derive(Clone, Copy, Eq, PartialEq, Debug)]
pub struct GameNode {
    state: GameState,
}

impl GameNode {
    pub fn initial() -> GameNode {
        GameNode {
            state: GameState::initial(),
        }
    }

    pub fn from_string(s: &str) -> Result<GameNode> {
        let state = GameState::from_string(s)?;
        Ok(GameNode { state })
    }

    pub fn at(&self, idx: u32) -> State {
        assert!(idx > 0 && idx <= 50);
        self.state.get(field_mask!(idx - 1))
    }

    pub fn movenumber(&self) -> (u32, Side) {
        (self.state.movenumber() + 1, self.state.side_to_move())
    }

    pub fn next(&self) -> Moves {
        self.state.find_moves()
    }

    pub fn rich_next(&self) -> RichMoves {
        self.state.find_moves()
    }

    pub fn find_matching_rich(&self, mv: Move) -> RichMove {
        self.rich_next()
            .iter()
            .find(|rm| rm.base == mv)
            .unwrap()
            .clone()
    }

    pub fn is_regulatory_draw(&self) -> bool {
        self.state.is_regulatory_draw()
    }

    pub fn winner(&self) -> Option<Winner> {
        let nxt = self.next();
        if !nxt.is_empty() {
            None
        } else if self.state.is_regulatory_draw() {
            Some(Winner::Draw("regulatory draw"))
        } else {
            match self.state.side_to_move() {
                Side::White => Some(Winner::Black),
                Side::Black => Some(Winner::White),
            }
        }
    }

    #[must_use]
    pub fn apply(&self, mv: Move) -> GameNode {
        let mut new_state = self.state;
        new_state.apply(mv.mask);

        GameNode { state: new_state }
    }

    #[must_use]
    pub fn apply_rich(&self, mv: RichMove) -> GameNode {
        self.apply(mv.base)
    }

    // abstraction leak really, expose internal state directly, returning a triplet of white, black and queens.
    // this makes implementing some evauluators more efficient
    pub fn counts(&self) -> (u64, u64, u64) {
        (
            self.state.white_mask & BOARD_MASK,
            self.state.black_mask & BOARD_MASK,
            self.state.queen_mask & BOARD_MASK,
        )
    }

    pub fn hash_val(&self) -> GameNodeHashVal {
        GameNodeHashVal {
            white: (self.state.white_mask & BOARD_MASK) | (self.state.white_mask & (1u64 << 50)),
            black: self.state.black_mask & BOARD_MASK,
            queens: self.state.queen_mask & BOARD_MASK,
        }
    }
}

impl fmt::Display for GameNode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.state.fmt(f)
    }
}

#[derive(Clone, Copy, Eq, PartialEq, Hash)]
pub struct GameNodeHashVal {
    white: u64,
    black: u64,
    queens: u64,
}

pub struct GameNodeBuilder {
    state: GameState,
}

impl GameNodeBuilder {
    pub fn initial() -> GameNodeBuilder {
        GameNodeBuilder {
            state: GameState::initial(),
        }
    }

    pub fn empty() -> GameNodeBuilder {
        GameNodeBuilder {
            state: GameState {
                white_mask: 0,
                black_mask: 0,
                queen_mask: 0,
            },
        }
    }

    #[must_use]
    pub fn with_piece(mut self, idx: u32, state: State) -> GameNodeBuilder {
        assert!(idx > 0 && idx <= 50);
        self.state.set(field_mask!(idx - 1), state);
        self
    }

    #[must_use]
    pub fn with_move(mut self, movenumber: u32, side: Side) -> GameNodeBuilder {
        assert!(movenumber > 0);

        // inefficient, but hey...
        while self.state.movenumber() + 1 != movenumber || self.state.side_to_move() != side {
            self.state.advance();
            self.state.mark_regulatory_draw();
        }
        self
    }

    pub fn build(self) -> GameNode {
        GameNode { state: self.state }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use quickcheck::{quickcheck, Arbitrary, Gen};

    #[test]
    fn initial_state() {
        let game = GameState::initial();

        assert_eq!(0, game.raw_movenumber());
        assert_eq!(0, game.movenumber());
        assert_eq!(Side::White, game.side_to_move());

        println!("{}", game);
        for i in 0..20 {
            assert_eq!(State::Black, game.get(as_field_idx(i)));
        }
        for i in 20..30 {
            assert_eq!(State::Empty, game.get(as_field_idx(i)));
        }
        for i in 30..50 {
            assert_eq!(State::White, game.get(as_field_idx(i)));
        }
    }

    #[test]
    fn set_to_state() {
        let mut game = GameState::initial();
        let idx = as_field_idx(20);

        assert_eq!(State::Empty, game.get(idx));

        game.set(idx, State::White);
        assert_eq!(State::White, game.get(idx));

        game.set(idx, State::Black);
        assert_eq!(State::Black, game.get(idx));

        game.set(idx, State::WhiteQueen);
        assert_eq!(State::WhiteQueen, game.get(idx));

        game.set(idx, State::BlackQueen);
        assert_eq!(State::BlackQueen, game.get(idx));

        game.set(idx, State::Empty);
        assert_eq!(State::Empty, game.get(idx));

        // nothing else has changed...
        let reference = GameState::initial();
        for i in 0..50 {
            let i = as_field_idx(i);
            assert_eq!(reference.get(i), game.get(i));
        }
    }

    #[test]
    fn advance_moves() {
        let mut game = GameState::initial();
        for _ in 0..10 {
            let last_raw = game.raw_movenumber();
            let last_move = game.movenumber();
            let last_side = game.side_to_move();

            game.advance();

            assert_eq!(last_raw + 1, game.raw_movenumber());
            match last_side {
                Side::White => {
                    assert_eq!(last_move, game.movenumber());
                    assert_eq!(Side::Black, game.side_to_move());
                }
                Side::Black => {
                    assert_eq!(last_move + 1, game.movenumber());
                    assert_eq!(Side::White, game.side_to_move());
                }
            }
        }
    }

    #[test]
    fn apply_simple_move() {
        let mut game = GameState::initial();
        let from = as_field_idx(32);
        let to = as_field_idx(28);

        assert_eq!(State::White, game.get(from));
        assert_eq!(State::Empty, game.get(to));

        game.apply(from | to);

        assert_eq!(State::White, game.get(to));
        assert_eq!(State::Empty, game.get(from));

        game.set(from, State::Black);
        game.set(to, State::Empty);

        game.apply(from | to);

        assert_eq!(State::Black, game.get(to));
        assert_eq!(State::Empty, game.get(from));

        game.set(from, State::WhiteQueen);
        game.set(to, State::Empty);

        game.apply(from | to);

        assert_eq!(State::WhiteQueen, game.get(to));
        assert_eq!(State::Empty, game.get(from));
    }

    #[test]
    fn apply_and_advance() {
        let mut game = GameState::initial();
        let from = as_field_idx(18);
        let to = as_field_idx(23);

        game.apply(field_mask!(33, 28));
        game.apply(field_mask!(18, 23));
        game.apply(field_mask!(32, 27));

        assert_eq!(State::Black, game.get(to));
        assert_eq!(State::Empty, game.get(from));

        assert_eq!(Side::Black, game.side_to_move());
        assert_eq!(3, game.raw_movenumber());
    }

    #[test]
    fn apply_capture() {
        let string = "White 1
        | |.| |.| |b| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |b| |.| |.| |
        | |.| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();
        let mvs = game.next();

        assert_eq!(1, mvs.len());
        assert_eq!(field_mask!(32, 27, 21), mvs[0].mask);
        let game = game.apply(mvs[0]);

        let expect = "Black 1
        | |.| |.| |b| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |w| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";
        assert_eq!(GameNode::from_string(expect).unwrap(), game);
    }

    #[test]
    fn apply_promote() {
        let string = "Black 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |b|
        |.| |.| |w| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();
        let mvs = game.next();

        assert_eq!(1, mvs.len());
        assert_eq!(field_mask!(44, 49), mvs[0].mask);

        let game = game.apply(mvs[0]);

        let expect = "White 2
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |w| |.| |B| |
        ";
        assert_eq!(GameNode::from_string(expect).unwrap(), game);
    }

    #[test]
    fn from_string_init() {
        let initial = GameState::initial();
        let init_str = format!("{}", initial);
        let game = GameState::from_string(init_str.as_str()).unwrap();

        println!("{}", initial);

        assert_eq!(0, game.movenumber());
        assert_eq!(Side::White, game.side_to_move());

        for field in 0..50 {
            let field = as_field_idx(field);
            assert_eq!(initial.get(field), game.get(field));
        }
    }

    #[test]
    fn from_string_custom() {
        let string = "Black 5
        | |W| |.| |b| |.| |.|
        |.| |b| |.| |.| |w| |
        | |B| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |B| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameState::from_string(string).unwrap();

        assert_eq!(State::White, game.get(as_field_idx(9)));
        assert_eq!(State::WhiteQueen, game.get(as_field_idx(0)));
        assert_eq!(State::Black, game.get(as_field_idx(2)));
        assert_eq!(State::BlackQueen, game.get(as_field_idx(41)));
        assert_eq!(State::Empty, game.get(as_field_idx(1)));
    }

    fn sorted(mut mvs: Moves) -> Moves {
        mvs.0.sort_by(|a, b| a.mask.cmp(&b.mask));
        mvs
    }

    #[test]
    fn find_moves_simple_white() {
        let string = "White 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |w| |.| |.| |.| |.| |
        | |b| |b| |.| |.| |w|
        |.| |b| |.| |.| |.| |
        | |w| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();

        let mvs = sorted(game.next());

        assert_eq!(5, mvs.len());
        assert_eq!(field_mask!(15, 10), mvs[0].mask);
        assert_eq!(field_mask!(24, 19), mvs[1].mask);
        assert_eq!(field_mask!(30, 25), mvs[2].mask);
        assert_eq!(field_mask!(32, 27), mvs[3].mask);
        assert_eq!(field_mask!(32, 28), mvs[4].mask);
    }

    #[test]
    fn find_moves_simple_black() {
        let string = "Black 1
        | |.| |.| |.| |.| |b|
        |b| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |b| |.| |b| |
        | |.| |.| |.| |w| |.|
        |.| |.| |.| |w| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();

        let mvs = sorted(game.next());

        assert_eq!(5, mvs.len());
        assert_eq!(field_mask!(4, 9), mvs[0].mask);
        assert_eq!(field_mask!(5, 10), mvs[1].mask);
        assert_eq!(field_mask!(17, 21), mvs[2].mask);
        assert_eq!(field_mask!(17, 22), mvs[3].mask);
        assert_eq!(field_mask!(19, 24), mvs[4].mask);
    }

    #[test]
    fn find_white_single_capture() {
        let string = "White 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |b| |.| |.| |
        | |.| |.| |w| |.| |.|
        |.| |.| |.| |b| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();

        let mvs = sorted(game.next());

        assert_eq!(2, mvs.len());
        assert_eq!(field_mask!(32, 27, 21), mvs[0].mask);
        assert_eq!(field_mask!(32, 38, 43), mvs[1].mask);
    }

    #[test]
    fn find_black_single_capture() {
        let string = "Black 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |w| |.| |.| |
        | |.| |.| |b| |.| |.|
        |.| |.| |.| |w| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();

        let mvs = sorted(game.next());

        assert_eq!(2, mvs.len());
        assert_eq!(field_mask!(32, 27, 21), mvs[0].mask);
        assert_eq!(field_mask!(32, 38, 43), mvs[1].mask);
    }

    #[test]
    fn find_multi_capture() {
        let string = "White 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |b| |
        | |.| |.| |.| |.| |w|
        |.| |b| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |b| |b| |.| |.| |
        | |w| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();

        let mvs = sorted(game.next());

        mvs.iter().for_each(|m| {
            println!("move: {} {:?}", m.num_removed(), to_field_vec(m.mask));
        });

        assert_eq!(2, mvs.len());
        assert_eq!(field_mask!(30, 26, 16, 10), mvs[0].mask);
        assert_eq!(field_mask!(32, 27, 16, 10), mvs[1].mask);
    }

    #[test]
    fn find_multi_capture_circular() {
        let string = "Black 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |w| |w| |w| |.|
        |.| |.| |.| |.| |.| |
        | |.| |w| |w| |w| |.|
        |.| |b| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();

        let mvs = sorted(game.next());

        mvs.iter().for_each(|m| {
            println!("move: {} {:?}", m.num_removed(), to_field_vec(m.mask));
        });

        assert!(!mvs.is_empty());
        mvs.iter().for_each(|m| {
            assert_eq!(field_mask!(26, 21, 12, 13, 23, 22, 11, 6), m.mask);
        });
    }

    #[test]
    fn find_moves_queen() {
        let string = "White 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |W| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |b| |.| |.| |.| |
        | |b| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();

        let mvs = sorted(game.next());

        println!("{}", game);
        println!("{}", game.rich_next());

        assert_eq!(8, mvs.len());
        assert_eq!(field_mask!(2, 8), mvs[0].mask);
        assert_eq!(field_mask!(3, 8), mvs[1].mask);
        assert_eq!(field_mask!(8, 12), mvs[2].mask);
        assert_eq!(field_mask!(8, 13), mvs[3].mask);
        assert_eq!(field_mask!(8, 17), mvs[4].mask);
        assert_eq!(field_mask!(8, 19), mvs[5].mask);
        assert_eq!(field_mask!(8, 21), mvs[6].mask);
        assert_eq!(field_mask!(8, 24), mvs[7].mask);
    }

    #[test]
    fn find_moves_capture_straight_line() {
        let string = "White 1
        | |.| |.| |.| |W| |.|
        |.| |.| |.| |.| |b| |
        | |.| |.| |.| |.| |.|
        |.| |.| |b| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |b| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();

        let mvs = sorted(game.next());
        println!("{}", game.rich_next());

        assert_eq!(2, mvs.len());
        assert_eq!(field_mask!(3, 17, 26, 30), mvs[0].mask);
        assert_eq!(field_mask!(3, 17, 26, 35), mvs[1].mask);
    }

    #[test]
    fn find_moves_capture_sideways() {
        let string = "Black 1
        | |.| |.| |.| |B| |.|
        |.| |.| |.| |.| |w| |
        | |.| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |W| |.| |.| |.| |
        | |.| |.| |w| |.| |.|
        |.| |.| |.| |.| |w| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let game = GameNode::from_string(string).unwrap();

        let mvs = sorted(game.next());

        println!("{}", game);
        mvs.iter().for_each(|m| {
            println!("move: {} {:?}", m.num_removed(), m);
        });

        assert_eq!(3, mvs.len());
        assert_eq!(field_mask!(3, 9, 15, 26, 32), mvs[0].mask);
        assert_eq!(field_mask!(3, 9, 20, 26, 32), mvs[1].mask);
        assert_eq!(field_mask!(3, 12, 32, 39, 34), mvs[2].mask);
    }

    #[test]
    fn game_nodes_are_hashable() {
        // hashing takes position and side to move into account, but not move numbers
        let string1 = "White 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |b| |
        | |.| |.| |.| |.| |w|
        |.| |b| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |b| |b| |.| |.| |
        | |w| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        ";
        let string2 = "Black 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |b| |
        | |.| |.| |.| |.| |w|
        |.| |b| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |b| |b| |.| |.| |
        | |w| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        ";
        let string3 = "White 2
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |b| |
        | |.| |.| |.| |.| |w|
        |.| |b| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |b| |b| |.| |.| |
        | |w| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        ";
        let string4 = "White 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |b| |
        | |.| |.| |.| |.| |w|
        |.| |b| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |b| |b| |.| |.| |
        | |w| |.| |w| |.| |.|
        |.| |.| |.| |w| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

        let mut hm: std::collections::HashSet<GameNodeHashVal> = std::collections::HashSet::new();

        assert!(hm.insert(GameNode::from_string(string1).unwrap().hash_val()));
        assert!(hm.insert(GameNode::from_string(string2).unwrap().hash_val()));
        assert!(!hm.insert(GameNode::from_string(string3).unwrap().hash_val()));
        assert!(hm.insert(GameNode::from_string(string4).unwrap().hash_val()));
    }

    #[test]
    fn bug_no_dst_field_when_already_visited_simplified() {
        let string = "White   47
        | |.| |.| |.| |.| |.|   1 -  5
        |.| |.| |.| |.| |.| |   6 - 10
        | |.| |.| |b| |b| |.|  11 - 15
        |.| |.| |.| |.| |.| |  16 - 20
        | |.| |b| |b| |b| |.|  21 - 25
        |.| |w| |.| |.| |.| |  26 - 30
        | |w| |.| |.| |.| |.|  31 - 35
        |.| |.| |.| |.| |.| |  36 - 40
        | |.| |.| |.| |.| |.|  41 - 45
        |.| |.| |.| |.| |.| |  46 - 50";

        let game = GameNode::from_string(string).unwrap();
        let next = game.next();
        assert_eq!(2, next.len());

        let _ = game.apply(next[0]); // internal assertions do not fire
    }

    #[test]
    fn bug_no_dst_field_when_already_visited() {
        let string = "White   47
        | |.| |b| |.| |.| |.|   1 -  5
        |.| |.| |.| |.| |.| |   6 - 10
        | |b| |.| |.| |.| |.|  11 - 15
        |.| |.| |.| |.| |W| |  16 - 20
        | |.| |.| |b| |b| |.|  21 - 25
        |.| |b| |.| |.| |w| |  26 - 30
        | |.| |.| |b| |w| |w|  31 - 35
        |.| |.| |.| |.| |.| |  36 - 40
        | |.| |.| |.| |.| |.|  41 - 45
        |w| |.| |.| |.| |.| |  46 - 50";

        let game = GameNode::from_string(string).unwrap();
        let next = game.next();

        next.iter().for_each(|m| {
            println!("move: {} {:?}", m.num_removed(), to_field_vec(m.mask));
        });

        assert_eq!(2, next.len());

        println!("{:?}", next[0]);

        let _ = game.apply(next[0]); // internal assertions do not fire
    }

    #[test]
    fn regulatory_draw_if_no_piece_moved_or_capture_in_25_moves() {
        let string = "White   1
        | |.| |B| |.| |.| |.|   1 -  5
        |.| |.| |.| |.| |.| |   6 - 10
        | |b| |.| |.| |.| |.|  11 - 15
        |.| |.| |.| |.| |.| |  16 - 20
        | |.| |.| |.| |.| |.|  21 - 25
        |.| |.| |.| |.| |.| |  26 - 30
        | |.| |.| |.| |.| |.|  31 - 35
        |.| |.| |.| |.| |.| |  36 - 40
        | |.| |.| |.| |.| |W|  41 - 45
        |.| |.| |.| |w| |.| |  46 - 50";

        let mut game = GameState::from_string(string).unwrap();

        for _ in 0..25 {
            game.apply(field_mask!(44, 39));
            game.apply(field_mask!(1, 7));
        }

        assert!(game.is_regulatory_draw());
        let mvs: Moves = game.find_moves();
        assert!(mvs.is_empty());
    }

    impl Arbitrary for Side {
        fn arbitrary(g: &mut Gen) -> Side {
            *g.choose(&[Side::White, Side::Black]).unwrap()
        }
    }

    impl Arbitrary for State {
        fn arbitrary(g: &mut Gen) -> State {
            *g.choose(&[
                State::White,
                State::Black,
                State::WhiteQueen,
                State::BlackQueen,
                State::Empty,
            ])
            .unwrap()
        }
    }

    impl Arbitrary for GameNode {
        fn arbitrary(g: &mut Gen) -> GameNode {
            let template = format!(
                "{:?} {}
                | |.| |.| |.| |.| |.|
                |.| |.| |.| |.| |.| |
                | |.| |.| |.| |.| |.|
                |.| |.| |.| |.| |.| |
                | |.| |.| |.| |.| |.|
                |.| |.| |.| |.| |.| |
                | |.| |.| |.| |.| |.|
                |.| |.| |.| |.| |.| |
                | |.| |.| |.| |.| |.|
                |.| |.| |.| |.| |.| |
                ",
                Side::arbitrary(g),
                std::cmp::max(1, u8::arbitrary(g))
            );
            let mut game = GameNode::from_string(template.as_str()).unwrap();

            for idx in 0..5 {
                game.state.set(
                    idx,
                    *g.choose(&[
                        State::WhiteQueen,
                        State::Black,
                        State::BlackQueen,
                        State::Empty,
                    ])
                    .unwrap(),
                );
            }
            for idx in 5..45 {
                game.state.set(idx, State::arbitrary(g));
            }
            for idx in 45..50 {
                game.state.set(
                    idx,
                    *g.choose(&[
                        State::WhiteQueen,
                        State::White,
                        State::BlackQueen,
                        State::Empty,
                    ])
                    .unwrap(),
                );
            }

            game
        }
    }

    quickcheck! {
        fn increment_count_on_move(game: GameNode) -> bool {
            let (old_number, old_side) = game.movenumber();
            game.next().iter().all(|mv| {
                let next_state = game.apply(*mv);
                let (new_number, new_side) = next_state.movenumber();

                let n = match new_side {
                    Side::White => new_number == old_number + 1,
                    Side::Black => new_number == old_number
                };
                n && new_side != old_side
            })
        }
    }
}
