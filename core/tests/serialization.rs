use dammen_core::{
    game::{GameNode, Side, State},
    serialization::{dump, load_from_string},
};

fn load(raw_data: &str) -> GameNode {
    load_from_string(raw_data).expect("invalid format")
}

fn output(game: GameNode) {
    let mut v = Vec::<u8>::new();
    dump(&mut v, game, &[], &[]).unwrap();
    let s = String::from_utf8(v).unwrap();
    println!("{}", s);
}

#[test]
fn initial_state() {
    let raw_data = r#"{"game": {}}"#;
    let game = load(raw_data);

    assert_eq!(GameNode::initial(), game);
    assert_eq!((1, Side::White), game.movenumber());
}

#[test]
fn custom_board() {
    output(GameNode::initial());

    let raw_data = r#"
        game:
            board:
                33: White
                28: Black
                23: WhiteQueen
                18: Empty
                17: BlackQueen
        "#;
    let game = load(raw_data);

    for idx in 1..51 {
        let piece = game.at(idx);
        match idx {
            33 => assert_eq!(State::White, piece),
            28 => assert_eq!(State::Black, piece),
            23 => assert_eq!(State::WhiteQueen, piece),
            17 => assert_eq!(State::BlackQueen, piece),
            _ => assert_eq!(State::Empty, piece),
        };
    }
}

#[test]
fn custom_move_and_side() {
    let raw_data = r#"
        game:
            board:
                33: White
                28: Black
                23: WhiteQueen
                18: Empty
                17: BlackQueen
            side_to_move: Black
            move_number: 32
        "#;
    let game = load(raw_data);
    assert_eq!((32, Side::Black), game.movenumber());
}

#[test]
fn apply_move() {
    let raw_data = r#"
    game:
        moves_after:
            - "33-28"
            - "18-22"
            - "39-33"
            - "22-27"
            - "31x22"
            - "19-23"
            - "xxx"
            - "17x28x39x30"
            - "x"
            - "x29"
    "#;
    let game = load(raw_data);
    println!("{}", game);

    let expect = "White    6
        | |b| |b| |b| |b| |b|   1 -  5
        |b| |b| |b| |b| |b| |   6 - 10
        | |b| |b| |b| |b| |b|  11 - 15
        |b| |.| |.| |w| |.| |  16 - 20
        | |.| |.| |.| |.| |.|  21 - 25
        |.| |.| |.| |b| |.| |  26 - 30
        | |.| |w| |.| |.| |.|  31 - 35
        |w| |w| |w| |.| |w| |  36 - 40
        | |w| |w| |w| |w| |w|  41 - 45
        |w| |w| |w| |w| |w| |  46 - 50";

    let expect = GameNode::from_string(expect).unwrap();
    assert_eq!(expect, game);
}

#[test]
fn ok_if_log_is_valid() {
    let raw_data = r#"
    game:
        board:
            42: White
            43: White
            35: White
            37: White
            46: White
            20: Black
            44: White
            3: Black
            7: Black
            17: Black
            19: Black
            18: Black
            48: White
            1: Black
            5: Black
            11: Black
            31: White
            34: White
            38: White
            8: Black
            15: Black
            10: Black
            32: White
            16: Black
            2: Black
            4: Black
            45: White
            47: White
            23: Black
            49: White
            40: White
            6: Black
            36: White
            9: Black
            12: Black
            33: White
            50: White
            41: White
        side_to_move: White
        move_number: 4
        moves_before:
            - 33 - 28
            - 19 - 23
            - 28 x 19
            - 14 x 23
            - 39 - 33
            - 13 - 19
        moves_after: []
    "#;

    let game = load(raw_data);
    println!("{}", game);

    let expect = "White    4
    | |b| |b| |b| |b| |b|   1 -  5
    |b| |b| |b| |b| |b| |   6 - 10
    | |b| |b| |.| |.| |b|  11 - 15
    |b| |b| |b| |b| |b| |  16 - 20
    | |.| |.| |b| |.| |.|  21 - 25
    |.| |.| |.| |.| |.| |  26 - 30
    | |w| |w| |w| |w| |w|  31 - 35
    |w| |w| |w| |.| |w| |  36 - 40
    | |w| |w| |w| |w| |w|  41 - 45
    |w| |w| |w| |w| |w| |  46 - 50";

    let expect = GameNode::from_string(expect).unwrap();
    assert_eq!(expect, game);
}

#[test]
fn fail_if_log_is_invalid() {
    let raw_data = r#"
    game:
        board:
            42: White
            43: White
            35: White
            37: White
            46: White
            20: Black
            44: White
            3: Black
            7: Black
            17: Black
            19: Black
            18: Black
            48: White
            1: Black
            5: Black
            11: Black
            31: White
            34: White
            38: White
            8: Black
            15: Black
            10: Black
            32: White
            16: Black
            2: Black
            4: Black
            45: White
            47: White
            23: Black
            49: White
            40: White
            6: Black
            36: White
            9: Black
            12: Black
            33: White
            50: White
            41: White
        side_to_move: White
        move_number: 4
        moves_before:
            - 33 - 28
            - 19 - 23
            - 28 x 19
            - 14 x 23
            - 39 - 33
            - 10 - 14
        moves_after: []
    "#;

    assert!(load_from_string(raw_data).is_err());
}
