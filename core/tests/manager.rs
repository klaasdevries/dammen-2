use dammen_core::{
    game::{GameNode, Move, Side, Winner},
    manager::{Context, GameManager, Player},
};
use std::sync::mpsc;

struct FakePlayer {
    out: mpsc::Sender<GameNode>,
}

impl Player for FakePlayer {
    fn choose(&mut self, game: &GameNode, _: &Context) -> Option<Move> {
        self.out.send(*game).unwrap();

        // silly helper, forfeit the game on move 42
        if game.movenumber().0 == 42 {
            return None;
        }

        game.next().iter().copied().next()
    }
}

struct Fixture {
    white_out: mpsc::Receiver<GameNode>,
    black_out: mpsc::Receiver<GameNode>,
    manager: GameManager,
}

impl Fixture {
    fn new(start: GameNode) -> Fixture {
        let (wtx, wrx) = mpsc::channel();
        let (btx, brx) = mpsc::channel();
        let manager = GameManager::with_initial(
            Box::new(FakePlayer { out: wtx }),
            Box::new(FakePlayer { out: btx }),
            start,
        );
        Fixture {
            white_out: wrx,
            black_out: brx,
            manager,
        }
    }

    fn white(&self, n: usize) -> impl Iterator<Item = GameNode> + '_ {
        (0..n).map(|_| self.white_out.recv().unwrap())
    }

    fn black(&self, n: usize) -> impl Iterator<Item = GameNode> + '_ {
        (0..n).map(|_| self.black_out.recv().unwrap())
    }
}

#[test]
fn alternates_player() {
    let mut f = Fixture::new(GameNode::initial());

    for _ in 0..6 {
        f.manager.advance();
    }

    let wnr: Vec<(u32, Side)> = f.white(3).map(|g| g.movenumber()).collect();
    let bnr: Vec<(u32, Side)> = f.black(3).map(|g| g.movenumber()).collect();

    assert_eq!(
        vec!((1, Side::White), (2, Side::White), (3, Side::White)),
        wnr
    );
    assert_eq!(
        vec!((1, Side::Black), (2, Side::Black), (3, Side::Black)),
        bnr
    );
}

#[test]
fn logs_moves() {
    let mut f = Fixture::new(GameNode::initial());

    for _ in 0..3 {
        f.manager.advance();
    }

    assert_eq!(&["34 - 30", "20 - 25", "39 - 34"], f.manager.get_log());
}

#[test]
fn returns_true_until_there_is_a_winner() {
    let start = "White 1
    | |.| |.| |.| |.| |.|
    |.| |.| |.| |.| |.| |
    | |.| |.| |.| |.| |.|
    |.| |b| |.| |.| |.| |
    | |b| |.| |.| |.| |.|
    |w| |b| |.| |.| |.| |
    | |.| |.| |.| |.| |.|
    |w| |.| |.| |.| |.| |
    | |.| |.| |.| |.| |.|
    |.| |.| |.| |.| |.| |
    ";
    let mut f = Fixture::new(GameNode::from_string(start).unwrap());

    assert_eq!(Winner::Black, f.manager.play());
}

#[test]
fn returns_false_if_player_forfeits() {
    let start = "White 41
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |w| |.| |.| |.| |w| |
        | |b| |b| |.| |.| |w|
        |.| |b| |.| |.| |.| |
        | |w| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";
    let mut f = Fixture::new(GameNode::from_string(start).unwrap());

    assert_eq!(None, f.manager.advance());
    assert_eq!(None, f.manager.advance());
    assert_eq!(Some(Winner::Black), f.manager.advance());
}
