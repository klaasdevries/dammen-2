use criterion::{criterion_group, criterion_main, Criterion};
use dammen_core::game::GameNode;

fn bench_find_moves(c: &mut Criterion, name: &str, game: GameNode) {
    c.bench_function(name, |b| b.iter(|| game.next()));
}

fn find_from_initial(c: &mut Criterion) {
    let game = GameNode::initial();
    bench_find_moves(c, "find from initial", game);
}

fn find_queen_captures(c: &mut Criterion) {
    let string = "Black 1
        | |.| |.| |.| |B| |.|
        |.| |.| |.| |.| |w| |
        | |.| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |W| |.| |.| |.| |
        | |.| |.| |w| |.| |.|
        |.| |.| |.| |.| |w| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

    bench_find_moves(
        c,
        "find queen captures",
        GameNode::from_string(string).unwrap(),
    );
}

fn find_queen_moves(c: &mut Criterion) {
    let string = "White 1
        | |.| |b| |.| |B| |.|
        |.| |.| |b| |b| |.| |
        | |.| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |W| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |b| |.| |.| |.| |.| |
        | |.| |.| |b| |.| |.|
        |.| |.| |.| |B| |.| |
        ";

    bench_find_moves(
        c,
        "find queen moves",
        GameNode::from_string(string).unwrap(),
    );
}

fn find_simple_moves(c: &mut Criterion) {
    let string = "White 1
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |w| |.| |.| |.| |.| |
        | |b| |b| |.| |.| |w|
        |.| |b| |.| |.| |.| |
        | |w| |.| |w| |.| |.|
        |.| |.| |.| |.| |.| |
        | |.| |.| |.| |.| |.|
        |.| |.| |.| |.| |.| |
        ";

    bench_find_moves(
        c,
        "find simple moves",
        GameNode::from_string(string).unwrap(),
    );
}

fn find_multi_capture(c: &mut Criterion) {
    let string = "White 1
    | |.| |.| |.| |.| |.|
    |.| |.| |.| |.| |b| |
    | |.| |.| |.| |.| |w|
    |.| |b| |.| |.| |.| |
    | |.| |.| |.| |.| |.|
    |.| |b| |b| |.| |.| |
    | |w| |.| |w| |.| |.|
    |.| |.| |.| |.| |.| |
    | |.| |.| |w| |.| |.|
    |.| |.| |.| |.| |.| |
    ";
    bench_find_moves(
        c,
        "find multi capture moves",
        GameNode::from_string(string).unwrap(),
    );
}

criterion_group!(
    benches,
    find_from_initial,
    find_simple_moves,
    find_multi_capture,
    find_queen_moves,
    find_queen_captures,
);
criterion_main!(benches);
