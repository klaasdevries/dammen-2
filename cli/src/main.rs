extern crate rustyline;

use clap::{Parser, ValueEnum};

use ai::{DefaultEvaluator, Evaluator, MTMinimaxAI, MinimaxAI, RandomAI};
use dammen_core::{
    game::{GameNode, Move, Winner},
    manager::{Context, GameManager, Player},
    serialization::{dump, find_matching_moves, load_from_file},
};

type Result<T> = anyhow::Result<T>;
use anyhow::Error;

#[derive(Clone, Copy, Debug, ValueEnum)]
enum PlayerType {
    /// Human (manual input)
    Human,
    /// AI
    AI,
    /// Multi-threaded AI (experimental)
    MtAi,
    /// Random
    Random,
}

/// dammen-2
#[derive(Parser, Debug)]
struct Args {
    /// player type for the white player
    #[arg(short, long, value_enum, default_value = "human")]
    white_player: PlayerType,

    /// player type for the black player
    #[arg(short, long, value_enum, default_value = "ai")]
    black_player: PlayerType,

    /// load file containing a start position
    #[arg(short, long)]
    load: Option<String>,

    /// save game to this file afterwards
    #[arg(short, long)]
    save: Option<String>,

    /// depth of the ai
    #[arg(long, default_value = "7")]
    ai_depth: u32,
}

fn main() -> Result<()> {
    let args = Args::parse();

    let start = match args.load {
        None => GameNode::initial(),
        Some(file) => load_from_file(&file)?,
    };

    let white_player = make_player(args.white_player, args.ai_depth)?;
    let black_player = make_player(args.black_player, args.ai_depth)?;

    let mut manager = GameManager::with_initial(white_player, black_player, start);

    loop {
        println!("{}", manager.get_game());
        let winner = manager.advance();
        let l = manager.get_log().len();
        if l > 0 {
            println!("{}", manager.get_log()[l - 1]);
        }

        if let Some(winner) = winner {
            println!("{}", manager.get_game());

            if let Winner::Draw(reason) = winner {
                println!("Draw: {}", reason);
            } else {
                println!("{:?} wins", winner)
            }

            break;
        }
    }

    if let Some(filename) = args.save {
        let mut file = std::fs::File::create(filename)?;
        manager.save(&mut file)?;
    }
    Ok(())
}

fn make_player(typ: PlayerType, ai_depth: u32) -> Result<Box<dyn Player>> {
    match typ {
        PlayerType::Human => {
            let h = Human::new()?;
            Ok(Box::new(h))
        }
        PlayerType::Random => Ok(Box::new(RandomAI::new())),
        PlayerType::AI => Ok(Box::new(MinimaxAI::new(ai_depth))),
        PlayerType::MtAi => Ok(Box::new(MTMinimaxAI::new(ai_depth))),
    }
}

struct Human {
    repl: rustyline::Editor<GameRepl, rustyline::history::DefaultHistory>,
}

impl Human {
    fn new() -> Result<Human> {
        let mut repl =
            rustyline::Editor::<GameRepl, rustyline::history::DefaultHistory>::with_config(
                rustyline::config::Config::builder()
                    .auto_add_history(true)
                    .build(),
            )?;
        repl.set_helper(Some(GameRepl::new()));
        Ok(Human { repl })
    }
}

impl Player for Human {
    fn choose(&mut self, game: &GameNode, ctx: &Context) -> Option<Move> {
        self.repl.helper_mut().unwrap().game = *game;

        loop {
            let line = self
                .repl
                .readline("> ")
                .map_err(|e| eprintln!("{}", e))
                .ok()?;

            match self.repl.helper().unwrap().parse(line, ctx) {
                Err(e) => {
                    println!("{}", e)
                }
                Ok((true, _)) => (),
                Ok((false, mv)) => return mv,
            }
        }
    }
}

struct GameRepl {
    game: GameNode,
}

impl GameRepl {
    fn new() -> GameRepl {
        GameRepl {
            game: GameNode::initial(),
        }
    }

    fn parse(&self, s: String, ctx: &Context) -> Result<(bool, Option<Move>)> {
        use std::sync::OnceLock;

        struct Regexes {
            save: regex::Regex,
            show_moves: regex::Regex,
            give_up: regex::Regex,
            eval: regex::Regex,
        }

        static REGEX: OnceLock<Regexes> = OnceLock::new();
        let regexes = REGEX.get_or_init(|| Regexes {
            save: regex::Regex::new(r"^\s*save\s+(.*)$").unwrap(),
            show_moves: regex::Regex::new(r"^\s*show\s*$").unwrap(),
            give_up: regex::Regex::new(r"^\s*resign\s*$").unwrap(),
            eval: regex::Regex::new(r"^\s*eval\s*$").unwrap(),
        });

        if let Some(m) = regexes.save.captures(s.as_str()) {
            let filename = m.get(1).unwrap().as_str();
            println!("saving game to {}", filename);

            let mut file = std::fs::File::create(filename)?;
            dump(&mut file, self.game, ctx.log, &[])?;
            Ok((true, None))
        } else if regexes.show_moves.is_match(s.as_str()) {
            println!("{}", self.game.rich_next());
            Ok((true, None))
        } else if regexes.give_up.is_match(s.as_str()) {
            Ok((false, None))
        } else if regexes.eval.is_match(s.as_str()) {
            let value = DefaultEvaluator::default().evaluate(&self.game);
            println!("value: {}", value);
            Ok((true, None))
        } else {
            self.next_move(s)
                .map(|m| (false, Some(m)))
                .ok_or_else(|| Error::msg("no matching unique move"))
        }
    }

    fn next_move(&self, s: String) -> Option<Move> {
        let matches = find_matching_moves(self.game, s.as_str());
        match matches.len() {
            0 => None,
            1 => Some(matches[0].simple()),
            _ => None,
        }
    }
}

impl rustyline::completion::Completer for GameRepl {
    type Candidate = String;

    fn complete(
        &self,
        line: &str,
        _pos: usize,
        _ctx: &rustyline::Context<'_>,
    ) -> rustyline::Result<(usize, Vec<Self::Candidate>)> {
        let mut candidates: Vec<String> = find_matching_moves(self.game, line)
            .iter()
            .map(|m| m.to_string())
            .collect();
        candidates.sort();
        Ok((0, candidates))
    }
}

impl rustyline::hint::Hinter for GameRepl {
    type Hint = String;
}

impl rustyline::highlight::Highlighter for GameRepl {}
impl rustyline::validate::Validator for GameRepl {}
impl rustyline::Helper for GameRepl {}
