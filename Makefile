all: format lint test build

build:
	cargo build

format:
	cargo fmt

lint: check check-wasm clippy

check:
	cargo check --all-targets

check-wasm:
	cargo check --all-features --lib --target wasm32-unknown-unknown

clippy:
	cargo clippy --all-targets

test:
	cargo test

bench:
	cargo bench

install:
	cargo install --path cli/
	cargo install --path gui/
